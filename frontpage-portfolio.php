  <div class="container grid">
     
	    <div id="grid" class="row">
            
				<div class="block col-xs-6 col-sm-6 col-md-6 col-lg-6" data-groups='["all", "webandinteractive", "socialmedia"]'>
					<img src="./img/portfolio/Portfolio_thumb-STETSON2.jpg" alt="" class="img-responsive">
					<a class="overlay" href="stetson-150th.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson 150 Tour</h2>
					<span class="smallheaderdivider"></span>
					<p>Promotional campaign support / integration with existing website</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="block col-xs-6 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
			    <img src="./img/portfolio/integratedstructures.png" alt="" class="img-responsive">
					<a class="overlay" href="itt_es.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Electronic Systems</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="block col-xs-6 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb21_AetnaAE2016.jpg" alt="" class="img-responsive">
					<a class="overlay" href="aetna-ae.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna AE2016 Microsite</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal microsite design / development for human resources team</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="block col-xs-6 col-sm-6 col-md-6 col-lg-6" data-groups='["all", "socialmedia"]'>
					<img src="./img/portfolio/xylem.png" alt="" class="img-responsive">
					<a class="overlay" href="watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark Website</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="block col-xs-6 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "graphicdesign"]'>
					<img src="./img/portfolio/beyonce.png" alt="" class="img-responsive">
					<a class="overlay" href="beyonce.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Beyoncé Fragrances Website</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="block col-xs-6 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb22_VoyaBMW.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-bmw.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya Benchmark Wizard</h2>
					<span class="smallheaderdivider"></span>
					<p>Feature / benefit comparison tool for retirement plan administrators</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
          <!-- sizer -->
      <div class="col-xs-6 col-sm-3 col-md-3 shuffle_sizer"></div>          
          
          
	    </div><!-- /#grid -->
    </div><!-- /.container -->
    
<script>
$(function() {
// OPACITY OF BUTTON SET TO 0%
$(".overlay").css("opacity","0");
 
// ON MOUSE OVER
$(".block").hover(function () {
 
// SET OPACITY TO 70%
$(this).find(".overlay").stop().animate({
opacity: 1
}, "medium");
},

// ON MOUSE OUT
function () {
 
// SET OPACITY BACK TO 50%
$(this).find(".overlay").stop().animate({
opacity: 0
}, "slow");
});
});
</script>