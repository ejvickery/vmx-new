<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left">
				<table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">   
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="mc_elearning.php"> 
                        <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        
                        <a class="next" href="savings_score.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>ITT - Electronic Systems Capabilities</h2>
				<span class="redline"></span>
				<h1>More than just a PowerPoint: Redefining the capabilities presentation for ITT Electronic Systems.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat05_es-caps.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>In the right hands, PowerPoint can be a rich and powerful communication tool. More often than not, however, it can also be staid and boring. Without a dynamic presenter at the controls, slide based content often falls short of the mark.</p>
				<p>ITT Electronic Systems was seeking a mechanism to tell its story &ndash; from its vision to its core competencies &ndash; in an interactive and engaging format. Whether the target audience was a new hire, a sales prospect, or a tradeshow visitor, the story was pretty much the same.</p>
				<p>
With overall costs and redundant efforts an ever-present issue, ITT was hoping to find a single tool that could speak to each audience in a slick and engaging manner. Team VMX was tasked with the challenge.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_ITTES1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ITTES2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ITTES3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ITTES4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ITTES5.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ITTES6.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Starting with base PowerPoint slides, we designed a slick user interface that brought the presentation to life &ndash; integrating video, sound, and animation effects to create a unified multimedia experience.</li>
    			<li>Working in Adobe Flash, we developed a core presentation shell that could be deployed in various ways &ndash; via the web, as a laptop-based sales tool, or as a tradeshow kiosk &ndash; to meet the unique needs of each audience.</li>
    			<li>We implemented a combination of linear and non-linear navigational tools that allowed users to move through the content along their own path.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The completed piece was used in a variety of ways &ndash; far more than originally intended! From its initial tradeshow presentation roots, the piece became a hit with HR as they on-boarded new hires, with the sales team, and with key executives.</li>
    		</ul>
    	</div>
    </div>
        
    <div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left">
				<table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="browse CS-navtext">BROWSE</td>
						<td class="nextprev"><a class="previous" href="mc_elearning.php">   <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="savings_score.php">   
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    </div><!--/container-->

    
<div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb10_itt-fluid.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./itt_fluid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Fluid Technologies</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb18_irc.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./irc.php">
<table><tbody><tr><td>
					<h2 class="smallheader">IRC History Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal company history video presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya SEE Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
  
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
