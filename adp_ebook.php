<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-jane.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="halle_berry.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - ADP e-Book</h2>
				<span class="redline"></span>
				<h1>Having fun with a visual motif&hellip; and adding life to mundane content in the process.</h1>
				
			</div>
            
           <!-- <a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat16_adp.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Understanding the often complicated and nuanced details surrounding a financial benefits plan can often be a real barrier to employee participation. The challenge is to find ways to add interest to the content so that it becomes more readily approachable.</p>
				<p>When ADP needed to produce their “Road to Retirement”, an electronic guide for their Total Source Retirement Savings Plan, VMX was tasked with creating a visual design and navigational framework that was both fun and functional.</p>
				<p>Using the title as our point of departure, we developed a visual design with a robust roadway theme &ndash; navigational elements and content callouts all designed to reflect traffic signs and symbols. We developed the site in the form of a responsive design, infinite scroll page framework, with the six content segments vertically stacked. This allowed users to quickly move from section to section within the page, either through organic scrolling or by use of the embedded “previous / continue” navigational tools.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_ADP1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ADP2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ADP3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ADP4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_ADP5.png">
						 </div>
        </div><!-- Carousel nav - quote marks -->
                          <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with the ADP team to develop a visual design that reflected their “Road to Retirement” thematic.</li>
    			<li>We built the site using a responsive design HTML and jQuery development approach, providing a seamless user experience from desktop to tablet to mobile.</li>
    			<li>Utilized a single page, infinite scroll development approach that provided quick access to all content elements.</li>
    			<li>Worked with ADP and the plan provider to link to all outside content elements and online resources.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Despite a very compressed development cycle, VMX completed the site and managed all 3rd party integrations to meet the client’s timeframe.</li>
    		</ul>
    	</div>
    </div>
    
    
         	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-jane.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="halle_berry.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
  
    
<div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb12_voya-myom.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - My Orange Money</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb18_irc.jpg') no-repeat center; background-size: cover">
				<a class="overlay"  href="./irc.php">
<table><tbody><tr><td>
					<h2 class="smallheader">IRC HISTORY PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal company history video presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./voya-ebooks.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
