<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="browse CS-navtext">BROWSE</td>
						<td class="nextprev"><a class="previous" href="guess.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="adp_ebook.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - "Jane" Videos</h2>
				<span class="redline"></span>
				<h1>Telling the story of retirement&hellip; and how Voya is there at every step of the way.</h1>
				<a href="#" class="btn btn-casestudy btn-reverse" data-toggle="modal" data-target="#videomodal">View Video</a>
			</div>            
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat15_voya-jane.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Each year, Voya hosts an annual Corporate Markets National Training Conference in order to align messaging and sales goals for the year to come. Voya’s marketing team is tasked with developing the meetings central theme and the various communications elements that are to be used throughout the conference, primarily the meeting opener.</p>
				<p>For the 2014 meeting, the Voya team was seeking to develop a more interactive and engaging experience focusing on their “1 to 21” pathway approach to holistic retirement planning. The goal of the opening presentation was to illustrate how Voya’s retirement plans impact participants across various stages of their lives, underscoring the conference central theme &ndash; “Building For The Future”.</p>
				<p>With this in mind, Voya asked team VMX to help develop a creative approach that would bring life to this concept … to make it real. Our solution was to examine the life of a virtual participant &ndash; Jane &ndash; who was to be shown through various stages of her life. Through four separate sequences &ndash; starting with “young Jane” and concluding with “old Jane” &ndash; we sought to illustrate how the financial and retirement decisions made by young Jane affect the outcome for old Jane. Between each sequence, a presenter would come out and discuss Voya’s relevance in the story, relating Jane’s various life-stage actions to milestones on the pathway to retirement.</p>
				<p>We worked closely with Voya to develop the storyboard and scripting for each sequence. In order to keep costs down, we leveraged stock video footage rather than shoot live action scenes. We scoured available libraries to find a series of clips &ndash; ideally using the same actors &ndash; that could be stitched together to tell our story. The video clips and supporting screen visuals were married together with recorded voice over narration and music track to create the final piece.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Voya-jane1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-jane2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-jane3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-jane4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-jane5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with the Voya marketing team to develop the script and storyboard.</li>
    			<li>Developed an overall visual design to be used throughout the piece, including static backdrops to be used between each sequence.</li>
    			<li>Recorded the video narration and sourced all stock video clips to be used.</li>
    			<li>Composited all elements together in Adobe After Effects to create the final video segments.</li>
    			<li>Worked closely with event staff to ensure supplied files met all technical requirements.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The “Jane” meeting opener videos were a huge success, with several audience members expressing to the marketing team that they understood their role in the lives of plan participants in a much more literal way.</li>
    		</ul>
    	</div>
    </div>
    
    
        	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="guess.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="adp_ebook.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
   
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb12_voya-myom.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya - My Orange Money</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb18_irc.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./irc.php">
<table><tbody><tr><td>
					<h2 class="smallheader">IRC History Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal company history video presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya - SEE Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
   


<!-- MODALS --> 

<!-- modal -->
<div id="videomodal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/closebutton-video.png"></button>
        <h4 class="modal-title">VisionMix</h4>
      </div>
      <div class="modal-body">
				<div id="video3">
					<div id="youtubevideo" class="video-responsive">
						<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/YnMP_Om8EFc?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<script type="text/javascript">
$(function() {

  $("#videomodal").on('hidden.bs.modal', function (e) {

    // needed to find some way to remove video and then replace it because IE would close the div but continue to play sound


    $('iframe').hide();   // must hide the YouTube iframe first or closing it before playing will cause a black window in IE


    $('#video3').hide('fast');  // then hide the container div that has the video div that has the YouTube iframe in it

    var bringback = $("#youtubevideo").clone(true);   //clone the div that has the YouTube iframe and assign it to a variable

    $("#youtubevideo").remove(); // remove the div that has the YouTube iframe

    $("#video3").html(bringback);   // replace or recreate the div that has the YouTube iframe using the variable with cloned information
    });

    $("#videomodal").on('shown.bs.modal', function (e) {
        $('iframe').show();   // show the YouTube iframe                         
         $('#video3').show('fast');   // show the div that contains the YouTube video div
    });
}); 
</script>
 
