<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="balenciaga.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="voya-jane.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Guess Content Inserts</h2>
				<span class="redline"></span>
				<h1>Creating compelling &ndash; and seamless &ndash; content inserts for a third party website.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat14_guess.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>For Coty beauty, sometimes developing a brand specific website is counter productive. The brand licensor &ndash; Guess, for example &ndash; has its own distinctive brand site with an established design aesthetic and the team to support it. Why then create a stand-alone site just for fragrances?</p>
				<p>In such cases, the best path to telling your part of the story is to create an engaging content insert that can be added seamlessly to the licensor’s site. Team VMX has helped Coty do this with several brands, among them the example above for their Guess Seductive and Seductive Hommes products. The final pieces integrated seamlessly into the Guess brand site and contained product collection and fragrance information as well digital video clips of ad spots and b-roll footage.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_guess1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_guess2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_guess3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_guess4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_guess5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with the Coty brand team to identify relevant content components and determine the best way to utilize them for the insert.</li>
    			<li>Worked with the Guess site team to define technical requirements for the inserts.</li>
    			<li>With the picture complete, we designed and developed 2 separate inserts for the two products and supplied all files to the Guess site team for inclusion in the licensor site.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Though the product inserts come and go as new products are introduced, VMX continues to work with Coty to develop similar pieces.</li>
    		</ul>
    	</div>
    </div>
    
    
         	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="balenciaga.php">   <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        
                        <a class="next" href="voya-jane.php">  
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
  
    
    

    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - HALLE BERRY FRAGRANCES</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb08_glgf.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./glgf.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - MULTIBRAND WEBSITE</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for holiday fragrance promotion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">XYLEM WATERMARK</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
