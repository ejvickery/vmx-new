<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="stetson.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="itt_es.php">  
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Mastercard - e-Learning</h2>
				<span class="redline"></span>
				<h1>Helping a Fortune 500 company put a fresh face on their electronic distance learning platform.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat04_mc-elearn.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>A growing majority of today’s large corporations use some form of e-learning platform &ndash; either internally or via an external service provider &ndash; to help employees stay current with all manner of internal policies and procedures. Creating compelling content for these platforms is often a challenge.</p>
				<p>The “Contracts Policies and Procedures” training module shown here was created for MasterCard in 2014 to help employees learn the intricacies of developing business contracts using their newly acquired Ariba system. To be successful, the module needed to showcase both the ins and outs of the Ariba platform as well as the underlying corporate policies that the platform was intended to support.</p>
				<p>Beyond the simple delivery of content, MasterCard’s training team was looking to develop something that could be more engaging and sophisticated than previous content modules. An internal team had initially been tasked with creating the piece, but the internal client soon decided that a fresh creative approach was needed to bring the content to life. 
</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_MC-elearn1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_MC-elearn2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_MC-elearn3.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Working within existing MasterCard brand standards, we sought to create a rich look and feel for the piece, including the integration of various full motion video backgrounds.</li>
    			<li>We developed an e-learning module using Adobe Flash comprised of animated text, video and screen graphics synced with a full-length voice over narration track.</li>
    			<li>Designed for use as both a primary learning and reference tool, we integrated both a linear and non-linear navigation system. This allowed users to complete the course in one session or jump around the content to address specific questions.</li>
    			<li>We developed a “test-out” mechanism that allowed user to skip the content presentation and proceed directly to a knowledge assessment / scoring mechanism. User’s who did not score within the designated range were required to retake the full course.</li>
    			<li>The piece was developed to be SCORM 1.2 compliant, integrating seamlessly with MasterCard’s existing Cornerstone LMS platform.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Working in tandem with MasterCard’s internal training department, team VMX successfully deployed the new Module in just over 4 weeks &ndash; a tight timeframe by any measure.</li>
    		</ul>
    	</div>
    </div>
        
       	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="stetson.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="itt_es.php">   <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
     </div><!--/container-->
   
    <div class="container">
    
    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb12_voya-myom.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya - My Orange Money</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb20_voya-ebook.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./voya-ebooks.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya Standard e-Book</h2>
					<span class="smallheaderdivider"></span>
					<p>Standardized e-book template design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb05_es-caps.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./itt_es.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Electronic Systems</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
