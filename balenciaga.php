<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
   <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="myorangemoney.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="guess.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Balenciaga Fragrances</h2>
				<span class="redline"></span>
				<h1>Bringing new life to a prestigious brand’s online persona.</h1>
				
                <a class="btn btn-casestudy btn-reverse" href="http://balenciagafragrance.com/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                
			</div>
            
            
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat13_balenciaga.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Balenciaga Fragrances is one of Coty Beauty’s elite “prestige” brands &ndash; a luxury scent that closely parallels the licensor brand’s high-end product line. VMX helped Coty’s Paris-based, Balenciaga brand team build a new Drupal site in 2011 &ndash; working closely with their London-based agency and French ECRM partner. Following the launch, the site was transitioned to a local team to perform updates and maintenance.</p>
				<p>When the brand team moved to New York City in 2014, they once again called on VMX to help support the site, and &ndash; ultimately &ndash; to redesign it. The goal of the update was two-fold: bring the site’s visual design more closely in line with the updated look and feel parent, and migrate the site from the existing Drupal 6.19 environment to Coty’s new Drupal 7 framework. The updated visual design was developed to be responsive, presenting a seamless user experience across devices.</p>
				<p>As part of the update process, the site’s opt-in and “Hidden Garden” user databases needed to be transitioned from the French team to Coty’s U.S. partner. VMX worked seamlessly with all parties to ensure a smooth and efficient transition.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Balenciaga1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Balenciaga2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Balenciaga3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Balenciaga4.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>We developed an entirely new visual design for the site, leveraging new brand elements and advertising materials to ensure a seamless integration with other campaign efforts.</li>
    			<li>We built the site using a responsive design HTML and jQuery development approach, providing a seamless user experience from desktop to tablet to mobile.</li>
    			<li>We worked with the brand team to bring campaign content elements &ndash; such as the “Hidden Garden” &ndash; to life in an engaging, interactive format.</li>
    			<li>We worked in tandem with Coty’s supporting 3rd party partners and their platforms, transitioning all site related content and data to the new Drupal 7 platform.</li>
    			<li>We developed supporting graphic email templates designed to drive traffic to the site.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Despite a very compressed development cycle, VMX completed the site and managed all 3rd party integrations to meet the contest timeframe.</li>
    			<li>Post-launch, we continued to work with the Balenciaga brand team and their partners to ensure that the site and supporting campaign efforts were in sync.</li>
    		</ul>
    	</div>
    </div>
    
    
         	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="myorangemoney.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="guess.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    
    </div><!--/container-->
    
 
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Coty - Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb08_glgf.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./glgf.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Coty - Multibrand Website</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for holiday fragrance promotion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
  
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
