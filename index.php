<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>VisionMix</title>

<?php include "header.php" ?>




<!--this is a hack to make the slider images fade properly in some browsers; add image URLs here if you add more images to the slider-->
<style>
body:after{
    display:none;
    content: url(./img/BG_HOME_laptop_ipad_LT.jpg) url(./img/BG_HOME_desktop1_LT.jpg) url(./img/BG_HOME_lobby_LT.jpg) url(./img/BG_HOME_print_LT.jpg) url(./img/BG_HOME_desktop2_LT.jpg) url(./img/BG_HOME_ipad-iphone_LT.jpg) url(./img/BG_HOME_ipad_LT.jpg) url(./img/HOME_headline.png);
}
</style>

<script>
$(function(){  // $(document).ready shorthand
   $('#myCarousel').fadeIn(1000);
   $('#formfunction').fadeIn(500);
});
</script>



 <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel" data-interval="4000" data-pause="false" style="display:none;">
        <!-- Indicators 
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>-->

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
           <div id="formfunction" class="fill formfunction" style="background-image:url('./img/HOME_headline.png');height:800px;display:none;">
           
           <a class="scroll downarrow img-responsive" href="#hello">
           <img class="bottom" src="./img/Home_arrow_white.png" alt=""/> 
           <img class="top" src="./img/Home_arrow_red.png" alt=""/>
          
           </a>
           
           </div>
           
            
               
         
    
        
            <div class="item active">
             
                <div class="fill" style="background-image:url('./img/BG_HOME_laptop_ipad_LT.jpg');height:800px;">
                
                
                
                </div>
               
            </div> 
            
            <div class="item">
            
            
               
                <div class="fill" style="background-image:url('./img/BG_HOME_desktop1_LT.jpg');height:800px;"></div>
              
            </div>
            
            
            
            
            <div class="item">
              
                <div class="fill" style="background-image:url('./img/BG_HOME_print_LT.jpg');height:800px;"></div>
               
            </div>
            
            
            
            <div class="item">
              
                <div class="fill" style="background-image:url('./img/BG_HOME_lobby_LT.jpg');height:800px;"></div>
               
            </div>
           
            
            
            <div class="item">
               
                <div class="fill" style="background-image:url('./img/BG_HOME_desktop2_LT.jpg');height:800px;"></div>
              
            </div>
            
            
            <div class="item">
               
                <div class="fill" style="background-image:url('./img/BG_HOME_ipad_LT.jpg');height:800px;"></div>
              
            </div>
            
             <div class="item">
               
                <div id="lastimg" class="fill" style="background-image:url('./img/BG_HOME_ipad-iphone_LT.jpg');height:800px;"></div>
              
            </div>
            
            
            
    
        </div>

        <!-- Controls 
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>-->

    </header>
    
    
    <div id="hello" class="container">
    

      <div class="row homedescrip">
      
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
      
      <h2 class="smallheader">Hello, we're VisionMix.</h2>
<span class="smallheaderdivider"></span>


      <h1 id="about" class="about">A <span class="red">boutique digital agency</span> that blends exceptional creative, flawless execution and unparalleled service.</h1>

<h2 class="about italic">Innovation is our passion. Bringing ideas to life ... one pixel at a time. Take a moment to learn more about us or explore some of our featured projects below.
</h2>
</div><!--/col-->

      </div><!--/row-->
      
     

      
        </div><!--/container-->
        
<?php include "frontpage-portfolio.php" ?> 


<div class="container fadein">
<div id="viewall" class="buttonwrap row text-center">
  <a class="btn btn-default" href="./work.php">View All Work <!--<i class="fa fa-chevron-right"></i>--></a>
  </div></div>        
 
    <footer> 

<?php include "letsworktogether.php" ?>  
 
<?php include "footer.php" ?> 
  
      
 