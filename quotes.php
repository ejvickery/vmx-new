
       <div class="checkbg">
       <div class="container">
      
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="projectcasestudies">
     <h2 class="smallheader">Proven Performance</h2>
<span class="smallheaderdivider"></span>  


<div id="quotecarousel" class="carousel slide" data-interval="10000" data-ride="carousel">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#quotecarousel" data-slide-to="0" class="active"></li>
        <li data-target="#quotecarousel" data-slide-to="1"></li>
        <li data-target="#quotecarousel" data-slide-to="2"></li>
        <li data-target="#quotecarousel" data-slide-to="3"></li>
    </ol>   
    <!-- Carousel items -->
    <div class="carousel-inner">

        <div class="item active">
            <h2>The team at VisionMix provide extremely creative design solutions for me and my corporate clients, regardless of application, media (and time frame). They are first and foremost solvers of communications problems through the application of great design and are a valued member of my team.</h2>
						<p><b>Amy from voya</b> - Feedback from the “Jane” Sales Meeting Video Opener</p>
        </div>

        <div class="item">
            <h2>The team at VisionMix have continually provided extremely creative design solutions for me and my corporate clients, regardless of application, media (and almost always with a very aggressive - and sometimes unreasonable - time frame). They are first and foremost solvers of communications problems through the application of strong design and are a valued partner of my team.</h2>
						<p><b>David from Lazar Creative Services</b> - Longtime VisionMix client and strategic partner</p>
        </div>

        <div class="item">
            <h2>When we were looking for a partner that could help shape our web design, we turned to VisionMix and have been very happy with the decision. They continually do a great job of understanding our needs and helping us find cost-effective solutions. After more than 15 years working with them, I can say we have never been disappointed.</h2>
						<p><b>Jim from Voya Financial&reg;</b> - Longtime client partner</p>
        </div>

        <div class="item">
            <h2>At COTY, we rely on partnerships with firms like VisionMix to address both quick emergency requests as well as full site creative design and development. Throughout our 6 year relationship, they have been a truly valuable resource – delivering exceptional work handling each and every challenge we’ve presented them with grace and professionalism.</h2>
						<p><b>Jennifer from COTY</b> - Assistant Director of Digital Marketing</p>
        </div>

    </div>
    
    
    <!-- Carousel nav - quote marks -->
    <a class="carousel-control left">
      <img alt="Quotation Mark" class="img-responsive" src="./img/leftquote.png" />
    </a>
    <a class="carousel-control right">
       <img alt="Quotation Mark" class="img-responsive" src="./img/rightquote.png" />
    </a>
    
</div>




 
       </div><!--/col-->
  </div><!--/row-->
  </div><!--/container-->