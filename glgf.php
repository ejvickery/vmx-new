<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
   <?php include "overlayinclude.php" ?>

	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="aetna-plan.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        
                        <a class="next" href="beyonce.php">   
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Multibrand Website (GLGF)</h2>
				<span class="redline"></span>
				<h1>A host of holiday fragrance gift sets &ndash; all under one digital roof.</h1>
                
				 <a class="btn btn-casestudy btn-reverse" href="http://www.givelovegivefragrance.com/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                 
			</div>
            
           
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat08_glgf.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Coty Beauty maintains a broad portfolio of celebrity and brand name fragrances &ndash; most of which maintain their own online presence. Fragrance gift sets, a popular item in the holiday season, accounts for approximately 40% of their annual sales, most of which through mass market retailers. Driving these holiday sales, therefore, is of vital importance to the business.</p>
				<p>The ongoing problem facing Coty was that coordinating efforts across numerous online properties is very difficult to effectively manage. Consolidating holiday fragrance offerings within a single site would allow for a far more cohesive &ndash; and manageable &ndash; marketing initiative.</p>
				<p>In 2013, Team VMX was asked to design and develop a multi-brand fragrance website within Coty’s existing Drupal CMS platform. The site would have some shared features &ndash; such as a couponing component and a fragrance finder &ndash; that could be used to help drive consumers to the right product and, ultimately, to the point of purchase. Within this common framework, each individual fragrance house would also need to be able to express its own unique identity and product line. Ideally, the completed site would be designed to live on after the Christmas holiday season &ndash; meeting the needs of subsequent holidays and prime gift-giving occasions.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_GLGF1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_GLGF2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_GLGF3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_GLGF4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_GLGF5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed a responsive, global site shell with consistent site-wide navigation and select page graphics that could be easily re-skinned to meet the needs of any holiday season.</li>
    			<li>Within this shell, we designed a “Fragrance House” template page that included a unique page masthead that allowed each brand team to express the unique visual identity and messaging specific to their products.</li>
    			<li>Created “Gifts for Her” and “Gifts For Him” page templates that allowed for specific, high-priority fragrance brands to be promoted.</li>
    			<li>Built an interactive “fragrance finder” that allows users to narrow down product choices based on personality traits and / or scent type.</li>
    			<li>Designed a series of graphic, drive to web e-mail templates that were sent to specific, high-priority brand opt-in lists.</li>
    			<li>Developed a series of unique “cluster pages” that served as the targets of these emails, providing cross marketing between like products that Coty wanted to specifically promote.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Working under a tight timeframe, we successfully deployed the Phase 1 site elements ahead of the 2013 holiday season &ndash; helping Coty to drive overall sales.</li>
    			<li>In 2014, the core site was re-skinned to align with Coty’s updated marketing visuals.</li>
    			<li>VMX continues to update and maintain the product database and associated visuals.</li>
    		</ul>
    	</div>
    </div>
    
    
      	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="aetna-plan.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="beyonce.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
      
    
    <div class="container">
    

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb09_beyonce.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./beyonce.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - Beyoncé Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
