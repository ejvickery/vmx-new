<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-ppt.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="stetson-150th.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - Standard eBook</h2>
				<span class="redline"></span>
				<h1>Using the economy of scale to bring an interactive communication platform to Voya Financial’s plan participants.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i></a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat20_voya-ebook.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>From online tools to mobile apps, Voya Financial strongly embraces the use of technology to improve the way in which it communicates with plan participants. For their larger clients they go the extra mile, creating custom tools and platforms that reflect the specific requests of the given plan sponsor. Digital e-books are just such a tool, and Team VMX has worked with Voya (and ING before) over the years to field custom e-books for several of their large corporate market clients.</p>
				<p>The costs to design and develop these e-books, however, can sometimes prove to be a barrier. With this in mind, Voya partnered with VMX to develop a standardized template and development methodology that could be used to expedite the creation of each individual book.</p>
				<p>We began by comparing all previous e-books, noting points of commonality and highlighting areas that would need to be customized for each client. We also sought ways to refine and streamline the overall development process, allowing our team to create a basic shell &ndash; complete with custom colors and graphics &ndash; within a day.</p>
				<p>The template was designed to be responsive, using a tab-driven menu structure along the top of the page that collapsed into a pop-up menu on smaller screen formats. A masthead banner area on the homepage allowed each book to have its own unique look. Visuals within each tab were kept largely generic, with only colors and branding elements being modified. We used collapsible accordion boxes to accommodate site areas where the most content customization would be required. Each e-book is developed using static HTML, jQuery and CSS &ndash; allowing for it to be fielded into virtually any client-specific environment.</p>
				<p>Beyond the baseline template, we worked with Voya to develop a menu of items that could be further customized for an additional cost. In this way, project managers could work with their clients to accurately define requirements and associated costs.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook5.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ebook6.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed and developed a standard visual and navigational shell to be used across all e-books.</li>
    			<li>Used a responsive HTML, jQuery and CSS development approach that allowed us to efficiently modify common theme elements.</li>
    			<li>Developed an MS Excel content map intended to expedite the content input process.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Using the standardized template, VMX and Voya have worked together to create 8 customized e-book executions &ndash; each to the delight of the plan sponsor client.</li>
    			<li>We are currently in the process of developing 2 additional e-books.</li>
    			<li>VMX is currently exploring the integration of a CMS platform, allowing the e-books to be housed within a single platform. This will facilitate future content updates and the addition of enhanced feature and capabilities across the platform.</li>
    		</ul>
    	</div>
    </div>
    
     	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-ppt.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        
                        <a class="next" href="stetson-150th.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
    
   
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb16_adp.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="adp_ebook.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - ADP Ebook</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive retirement planning guide</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>

			</div>

		</div>

	</div>


<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
