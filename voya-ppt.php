<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="irc.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a></td>
						<td><a class="next" href="voya-ebooks.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - SEE Presentation</h2>
				<span class="redline"></span>
				<h1>Helping a long-time client find its new voice.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat19_voya-ppt.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>When ING transitioned to Voya in 2014 there was a mad dash to update all manner of internal and external communication materials &ndash; brochures, television advertising, websites, you name it. Team VMX was happily along for the ride, updating numerous tools, demos and online platforms developed for ING over the years.</p>
				<p>When the dust had settled and the highest-priority items were checked off the list, Voya began to examine some of their remaining needs. This included their library of seminar presentations, delivered to plan participants at the behest of plan sponsors to help boost employee engagement and participation. The challenge was that the perceived need went well beyond just updating the PowerPoint templates. The content was thought to be stale and in need of a wider review, and the rebrand provided an excuse to give the presentation a new voice.</p>
				<p>VMX worked closely with Voya and a new content development partner to review the existing slide visuals, breaking them down to their component parts. Our goal was to craft a new message … and a new way to tell it. We shied away from simply updating a series of text slides, opting instead to use visuals wherever possible and allowing the presenter to tell the story. Though we worked within the updated Voya brand guidelines, we tried to push the envelope in order to make a more compelling overall presentation.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt5.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-ppt6.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					<a class="carousel-control left"></a> <a class="carousel-control right"></a>
				</div>
                
                         <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
                
                
			</div><!--/col-->
            
      
                        
            
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked hand in hand with Voya content providers to craft a new and refined message.</li>
    			<li>Worked closely with Voya’s brand team, ensuring all new slide visuals were inline with the emerging brand aesthetic.</li>
    			<li>Provided PowerPoint production support, taking responsibility for updating the various decks and developing screen animations to add visual impact to the content.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The initial version of the updated content deck was successfully piloted to an internal audience and has passed through Voya’s internal compliance review process.</li>
    			<li>The creative approach and visual treatment used throughout the initial content deck is currently being applied across numerous audience and market specific presentations.</li>
    		</ul>
    	</div>
    </div>
    
       	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoveimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="browse CS-navtext">BROWSE</td>
						<td class="nextprev" ><a class="previous" href="irc.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a></td>
						<td><a class="next" href="voya-ebooks.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
   
    
    <div class="container">


    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb10_itt-fluid.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./itt_fluid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT - FLUID TECHNOLOGIES</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb12_voya-myom.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - MY ORANGE MONEY</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb20_voya-ebook.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./voya-ebooks.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
