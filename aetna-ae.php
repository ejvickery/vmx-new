<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="stetson-150th.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="voya-bmw.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Aetna - AE 2016 Microsite</h2>
				<span class="redline"></span>
				<h1>Delivering key employee benefits information in a colorful and approachable microsite</h1>
        <!--a class="btn btn-casestudy btn-reverse" href="http://visionmix.com/" target="_blank">Visit Site</a-->
			</div>
          
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat21_AetnaAE2016.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Aetna’s Human Resources Benefits and Shared Services business area is responsible for architecting and delivering annual healthcare benefits to the company’s 40k+ employees – a daunting task to say the least. A key component of the task each year is to effectively communicate the various changes and nuances from one year to the next so that employees can make wise and educated decisions for their families.</p>
				<p>In an effort to improve the process for the coming year, Aetna was seeking to develop and deploy an informational microsite ahead of the 2016 Annual Enrollment period that would provide early details and planning tools to the company’s employees. Much of the information would be contained with various plan documents and guides typically available in PDF format. The primary goal of the initiative, therefore, would be to efficiently drive users to the appropriate document and do so in an approachable and easy to understand fashion.</p>
				<p>Ideally the site would be developed using a CMS backend in order to facilitate the rapid content update process that often occurs as the benefit offerings – and supporting documentation – are finalized. Given that the site would be used both within the Aetna firewall and at home as employees discussed their benefits decisions with their families, the site had to be developed such that it would be equally accessible on a standard-issue company laptop as well as a mobile or tablet device typically found at home.</p>
				<p>As you can see, team VMX was more than up to the task.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_01.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_02.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_03.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_04.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_05.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_AetnaAE2016_06.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with Aetna’s internal communications and technical team members to architect a perfect and manageable technology solution.</li>
    			<li>Attended brand training to ensure all design and content elements were within corporate standards.</li>
    			<li>Designed and developed the microsite’s user interface and navigational shell to be approachable, simple and intuitive – driving users to the appropriate materials within a minimum number of clicks.</li>
    			<li>Built the site on the WordPress CMS platform – allowing both VMX and Aetna team members to collaboratively maintain the site’s content and documents..</li>
    			<li>Utilized a responsive design HTML and jQuery development approach, providing a seamless user experience from desktop to tablet to mobile.</li>
    			<li>Creating various web-banner assets to be used within supporting / companion sites within Aetna to help generate awareness and drive traffic to the site.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>As launch day approached, VMX was on hand to help Aetna’s IT team to fully load test, debug and troubleshoot the completed site as it was migrated to the company’s cloud hosting environment.</li>
    			<li>On launch day, the site received rave reviews from both Aetna management and employees alike – handling traffic from over 16k unique visitors without a single service disruption.</li>
    			<li>2 days post-launch, VMX processed and uploaded 6 video segments from the AE2016 town hall event at which the site was unveiled.</li>
    			<li>Team VMX continues to work with Aetna’s internal content and technical team members to address issues and perform content maintenance.</li>
    		</ul>
    	</div>
    </div>
    
    
           	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="stetson-150th.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="voya-bmw.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
    
 
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>
    
		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
