<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>VisionMix</title>

<?php
$page = 'page1';
include "header.php" ?>

    

<div class="row colorblockheader redbg equalHeight">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 desktopbg left">
		<table>
			<tbody>
				<tr>
					<td><h1><span class="red">Big City</span> <span class="white">Work. Small town charm.</span></h1></td>
				</tr>
			</tbody>
		</table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
  	<table>
  		<tbody>
  			<tr>
  				<td><p class="italic">Conveniently located between New York and Boston, we are proud to offer our clients the caliber of work typically found in a big city agency - just with alot less hassle. From websites to brochures to animated videos to interactive presentations, we’ve done it all. Take a look through our project case studies or stop by for a visit. Be sure to bring your flip flops and beach towel.</p>
					</td>
  			</tr>
  		</tbody>
  	</table>
	</div>
</div>
      
    

<?php include "work-portfolio.php" ?> 


<?php include "quotes.php" ?> 


<?php include "client-list.php" ?>   


<footer>
<?php include "shallwedance.php" ?> 
<?php include "footer.php" ?> 
  
      
 