<div id="case-study" class="container fadein gridcontainer text-center">


<h2 class="smallheader">Project Case Studies</h2>
<span class="smallheaderdivider"></span>   
 

          
          <div class="row">
      
         
         
       

<script>
$(window).bind("load", function () {
    var $lineMenu = $("#work-nav"),
        $active = $lineMenu.children('.active'),
        $magicLine = $("<li id='magic-line'></li>");

    $magicLine
        .width($active.width())
        .css("left", $active.position().left)
        .appendTo($lineMenu);

    $lineMenu.on('mouseenter mouseleave click', 'li:not(#work-nav)', function (e) {
        var $el = e.type == 'mouseleave' ? $active : $(this);

        if (e.type == 'click') {
            $el.addClass('active').siblings().removeClass('active');
            $active = $el;
        } else {
            $magicLine.stop().animate({
                left: $el.position().left,
                width: $el.width()
            });
        }
    });
});</script>


 
<nav role="navigation">
    <div class="container-fluid navwrap">
      
        <div class="navbar-collapse" id="portfolio-nav">
            <ul class="nav navbar-nav center filter-options" id="work-nav">
                
            
                
                
                <li class="active" data-group="all">
                     <a>All</a>
         
                </li>
                <li data-group="webandinteractive">
                    <a>Web &amp; Interactive</a>
        
                </li>
                
                <li data-group="motiongraphics">
                    <a>Motion Graphics</a>
        
                </li>
                
                <li data-group="graphicdesign">
                    <a>Graphic Design</a>
        
                </li>
                
                
                <li data-group="socialmedia">
                    <a>Social Media</a>
        
                </li>
                
              
                
            </ul>
						

</div></div>
</nav>

</div>

   <div class="container grid">
     
      
            
	    <div id="grid" class="row">
		    
            
				<div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "socialmedia"]'>
					<img src="./img/CS_thumb24_Stetson150.jpg" alt="" class="img-responsive">
					<a class="overlay" href="stetson-150th.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson 150 Tour</h2>
					<span class="smallheaderdivider"></span>
					<p>Promotional campaign support / integration with existing website</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
				<div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb21_AetnaAE2016.jpg" alt="" class="img-responsive">
					<a class="overlay" href="aetna-ae.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna AE2016 Microsite</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal microsite design / development for human resources team</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
				<div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb22_VoyaBMW.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-bmw.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya Benchmark Wizard</h2>
					<span class="smallheaderdivider"></span>
					<p>Feature / benefit comparison tool for retirement plan administrators</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
				<div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb23_VoyaENROLLVID.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-enroll-vid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya Enrollment Promo</h2>
					<span class="smallheaderdivider"></span>
					<p>Promotion video design / development for new electronic enrollment tool</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "graphicdesign"]'>
			    <img src="./img/CS_thumb03_exelis.jpg" alt="" class="img-responsive">
					<a class="overlay" href="exelis.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Exelis Print</h2>
					<span class="smallheaderdivider"></span>
					<p>Print and collateral material design collection</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "socialmedia"]'>
					<img src="./img/CS_thumb02_watermark.jpg" alt="" class="img-responsive">
					<a class="overlay" href="watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark Website</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
				<div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "socialmedia"]'>
					<img src="./img/CS_thumb01_stetson.jpg" alt="" class="img-responsive">
					<a class="overlay" href="stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "motiongraphics"]'>
			    <img src="./img/CS_thumb04_mc-elearn.jpg" alt="" class="img-responsive">
					<a class="overlay" href="mc_elearning.php">
<table><tbody><tr><td>
					<h2 class="smallheader">MasterCard e-Learning</h2>
					<span class="smallheaderdivider"></span>
					<p>Distance learning module for LMS integration</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "motiongraphics"]'>
			    <img src="./img/CS_thumb05_es-caps.jpg" alt="" class="img-responsive">
					<a class="overlay" href="itt_es.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Electronic Systems</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
		    
		    <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb06_voya-score.jpg" alt="" class="img-responsive">
					<a class="overlay" href="savings_score.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya My Savings Score / State of Savings</h2>
					<span class="smallheaderdivider"></span>
					<p>Online calculator and interactive map component</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "graphicdesign"]'>
					<img src="./img/CS_thumb07_aetna-plan.jpg" alt="" class="img-responsive">
					<a class="overlay" href="aetna-plan.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna Plan Highlights</h2>
					<span class="smallheaderdivider"></span>
					<p>Employee benefits guide print piece and electronic companion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive", "socialmedia"]'>
					<img src="./img/CS_thumb08_glgf.jpg" alt="" class="img-responsive">
					<a class="overlay" href="glgf.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Coty Multibrand Website (GLGF)</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for holiday fragrance promotion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb09_beyonce.jpg" alt="" class="img-responsive">
					<a class="overlay" href="beyonce.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Beyoncé Parfums</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "graphicdesign"]'>
					<img src="./img/CS_thumb10_itt-fluid.jpg" alt="" class="img-responsive">
					<a class="overlay" href="itt_fluid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Fluid Technologies Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb11_aetna-vid.jpg" alt="" class="img-responsive">
					<a class="overlay" href="aetna-video.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna Wellness Video</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal employee benefits promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb12_voya-myom.jpg" alt="" class="img-responsive">
					<a class="overlay" href="myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya My Orange Money</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb13_balenciaga.jpg" alt="" class="img-responsive">
					<a class="overlay" href="balenciaga.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Balenciaga Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Luxury fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb14_guess.jpg" alt="" class="img-responsive">
					<a class="overlay" href="guess.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Guess Seductive</h2>
					<span class="smallheaderdivider"></span>
					<p>Design / development of licensor website content inserts</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb15_voya-jane.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-jane.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya "Jane" Videos</h2>
					<span class="smallheaderdivider"></span>
					<p>National Sales meeting video opener</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb16_adp.jpg" alt="" class="img-responsive">
					<a class="overlay" href="adp_ebook.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ADP e-Book</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive retirement planning guide</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb17_halle.jpg" alt="" class="img-responsive">
					<a class="overlay" href="halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "motiongraphics"]'>
					<img src="./img/CS_thumb18_irc.jpg" alt="" class="img-responsive">
					<a class="overlay" href="irc.php">
<table><tbody><tr><td>
					<h2 class="smallheader">IRC History Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal company history video presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "graphicdesign"]'>
					<img src="./img/CS_thumb19_voya-ppt.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya SEE Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
        <div class="portblock col-xs-12 col-sm-6 col-md-3 col-lg-3" data-groups='["all", "webandinteractive"]'>
					<img src="./img/CS_thumb20_voya-ebook.jpg" alt="" class="img-responsive">
					<a class="overlay" href="voya-ebooks.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya Standard e-Book</h2>
					<span class="smallheaderdivider"></span>
					<p>Standardized e-book template design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
					</a>
				</div>
            
          <!-- sizer -->
      <div class="col-xs-6 col-sm-3 col-md-3 shuffle_sizer"></div>          

</div>
</div>
</div>

<script>
$(function() {
// OPACITY OF BUTTON SET TO 0%
$(".overlay").css("opacity","0");
 
// ON MOUSE OVER
$(".portblock").hover(function () {
 
// SET OPACITY TO 70%
$(this).find(".overlay").stop().animate({
opacity: 1
}, "medium");
},

// ON MOUSE OUT
function () {
 
// SET OPACITY BACK TO 50%
$(this).find(".overlay").stop().animate({
opacity: 0
}, "slow");
});
});
</script>

