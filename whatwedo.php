<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>VisionMix</title>

<?php
$page = 'page3';
include "header.php" ?>


<div class="row colorblockheader redbg equalHeight">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 laptopbg left">
		<table>
			<tbody>
				<tr>
					<td><h1><span class="white">We create</span> <span class="red">digital solutions</span> <span class="white">for today's communication challenges.</span></h1></td>
				</tr>
			</tbody>
		</table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
  	<table>
  		<tbody>
  			<tr>
  				<td><p class="italic">Web. Print. Motion graphics. Social media. Our team of digital maestros are well versed in today’s communication technologies and platforms. Regardless of the scope of any particular engagement, every project we undertake is built upon a solid foundation of energy, responsiveness, passion, teamwork and insight focused on delivering compelling results.</p></td>
  			</tr>
  		</tbody>
  	</table>
	</div>
</div>

      
<?php include "servicesweoffer.php" ?>

<?php include "howwework.php" ?>
<?php include "ourprocess.php" ?> 

     
<footer>
<?php include "letsworktogether.php" ?> 
<?php include "footer.php" ?> 
  
      
 