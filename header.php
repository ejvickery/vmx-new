


	<link type="text/css" href="./css/bootstrap-custom.min.css" rel="stylesheet">

	<link type="text/css" href="./css/styles.css" rel="stylesheet">



	<link type="text/css" href="./css/styles_bub.css" rel="stylesheet">


	<link type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/6cdcb018-0bfa-450e-88ec-b702d8b9302a.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<script type="text/javascript" src="//code.jquery.com/jquery.min.js"></script>

<!-- 
<script type="text/javascript"  src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
 -->
<script type="text/javascript"  src="http://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.7.2.js"></script> 

<script type="text/javascript" src="https://rawgit.com/Vestride/Shuffle/master/dist/jquery.shuffle.min.js"></script>




    
</head>


<body style="position:relative;" data-spy="scroll" data-target=".scroll">

<div class="screensizer"></div>
    
<nav role="navigation" class="navbar navbar-fixed-top">
    <div class="container-fluid navwrap">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#my-nav" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">&nbsp;</span>
                <span class="icon-bar">&nbsp;</span>
                <span class="icon-bar">&nbsp;</span>
            </button>
            <a href="/" class="navbar-brand">
                <img alt="VisionMix" src="./img/vmxlogo.png" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="my-nav">
            <ul class="nav navbar-nav center" id="main-nav">
                <li class="<?php echo ($page == 'page1') ? 'active' : ''; ?>">
                    <a  href="./work.php">WORK</a>
                </li>
                <li class="<?php echo ($page == 'page2') ? 'active' : ''; ?>">
                    <a  href="./about.php">ABOUT</a>
                </li>
                <li class="<?php echo ($page == 'page3') ? 'active' : ''; ?>">
                    <a  href="./whatwedo.php">WHAT WE DO</a>
                </li>
                <li class="<?php echo ($page == 'page4') ? 'active' : ''; ?>">
                    <a href="./contact.php">CONTACT</a>
                </li>
              <!--   <li>
                    <a href="http://client.visionmix.com">CLIENT LOGIN</a>
                </li>-->
            </ul>
						
            <ul class="nav navbar-nav navbar-right">
                <li>
               	 <a href="https://www.facebook.com/VisionMixDigitalMedia" target="_blank" class="fa fa-facebook fa-lg"></a>
                </li>
                <li>
                 <a href="https://twitter.com/vmxdigital" target="_blank" class="fa fa-twitter fa-lg"></a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/visionmix-digital-media-&-design-inc-" target="_blank" class="fa fa-linkedin fa-lg"></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
