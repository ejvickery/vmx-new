<div class="container">
<div class="row text-center">

<h2 class="smallheader">Services We Offer</h2>
<span class="smallheaderdivider"></span>      
  </div><!--/row-->

<div class="row services text-center">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 webandinteractive">
<img alt="" class="img-responsive" src="./img/webandinteractive.png"/>
<h2>Web &amp; Interactive</h2>
<p>Our interactive team can help you design and develop your website, application, or presentation using today’s full range of tools and resources.</p>

<ul>
<li>Web / Intranet / Microsites</li>
<li>CMS / Ecommerce</li>
<li>Online Promotions / Web Banners</li>
<li>E-Learning / Training Programs</li>
<li>PowerPoint Presentations</li>
</ul>

</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 motiongraphics">
<img alt="" class="img-responsive" src="./img/motiongraphics.png"/>
<h2>Motion Graphics &amp; Video</h2>
<p>We have extensive experience in creating superior motion graphics and digital video elements - from live shoots to animation design to post-production.</p>
<ul>
<li>Adobe After Effects Design</li>
<li>2D / 3D Animation Design</li>
<li>Multimedia Presentations</li>
<li>Video to Web Processing</li>
<li>Voiceover Narration Record / Edit</li>

</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 graphicdesign">
<img alt="" class="img-responsive" src="./img/graphicdesign.png"/>
<h2>Graphic Design</h2>
<p>We produce powerfully designed print campaigns in both new media and traditional advertising that reinforces your brand message and genuinely resonates with your target audience.</p>
<ul>
<li>Marketing Slicks & Brochures</li>
<li>Business Cards</li>
<li>Direct Mail, Letterhead, & Envelopes</li>
<li>Print Advertising</li>
<li>Conference Signage</li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 socialmedia">
<img alt="" class="img-responsive" src="./img/socialmedia.png"/>
<h2>Social Media &amp; Web Marketing</h2>
<p>Our holistic approach generates measurable results by involving both brand managers and digital marketers to implement buzz worthy social campaigns that are SEO-friendly and reportable.</p>
<ul>
<li>Branding & Design</li>
<li>Campaign Development</li>
<li>Social Media Marketing</li>
<li>Reporting & Analytics Dashboards</li>
<li>Email Marketing</li>

</ul>

</div>

  </div><!--/row-->
  </div><!--/container-->