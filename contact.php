<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>VisionMix</title>

<?php
$page = 'page4';
include "header.php" ?>

<script src="./js/jquery.placeholder.min.js"></script>
<script>
$(function() {
$('input, textarea').placeholder({customClass: 'placeholder'});
});
</script>

      <div class="row checkbg smalltitle">
      
      
      <h1 class="contactheader"><span class="white">What can we do</span> <span class="red">for you?</span>
</h1>
      </div>
      
      
      
<div class="container formcontainer">

<div class="row text-center">

<p>You can reach us in many ways, but if you'd like to shoot us a quick message feel free to use the form below.  You can also send us an email at <a href="mailto:sales@visionmix.com">sales@visionmix.com</a> or give us a call at 203.292.6900.</p>

</div>

<div class="row">

 <form name="sentMessage" id="contactForm" action="./bin/contact_me.php"  novalidate="">
 	  <div id="success"></div>
         
		 <div class="control-group">
                    <div class="controls">
			<input type="text" class="form-control" 
            
			   	   placeholder="YOUR NAME" id="name" required
			           data-validation-required-message="!  Enter your name" />
			  <p class="help-block"></p>
		   </div>
	         </div> 	
                <div class="control-group">
                  <div class="controls">
			<input type="email" class="form-control" placeholder="YOUR EMAIL" 
			   	            id="email" required
			   		   data-validation-required-message="! Enter your email" />
		</div>
	    </div> 	
        
        <div class="control-group">
                  <div class="controls">
			<input type="text" class="form-control" placeholder="YOUR ORGANIZATION" 
			   	            id="organization" required
			   		   data-validation-required-message="! Enter your organization" />
		</div>
	    </div> 	
			  
               <div class="control-group">
                 <div class="controls">
				 <textarea rows="10" cols="100" class="form-control" 
                       placeholder="YOUR MESSAGE" id="message" required
		       data-validation-required-message="! Enter your message" minlength="5" 
                       data-validation-minlength-message="You must enter at least 5 characters" 
                        maxlength="999" style="resize:none"></textarea>
		</div>
               </div> 		 

	    <div class="buttonwrap text-center">
        <button type="submit" class="btn btn-default btn-submit">Send Message</button><br />
        </div>
          </form>
</div>

</div>



<script type="text/javascript" src="./js/jqBootstrapValidation.js"></script><script type="text/javascript" src="./js/contact_me.js"></script>


 <div class="  row colorblockheader lightgraybg" style="padding-top:0;">
      
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<h2 class="comesayhello">Come Say Hello.</h2>
<span class="hellodivider"></span>  

<h3 id="address">Visionmix Digital Media and Design. Inc.<br/>
2000 Post Road, Suite L101<br/>
Fairfield, Connecticut 06824</h3> 
<br/><br/>
 <p class="phoneandemail"><img alt="Phone:" class="" src="./img/phoneicon2.png"/> 203.292.6900  <br class="visible-xs visible-sm"/><img alt="Email:" class="" src="./img/emailicon2.png"/> <a href="mailto:sales@visionmix.com">sales@visionmix.com</a></p>



      </div>
      
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 map" id="map">
        
      
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" >
      </div><!--/col-->
     
      </div>
      
      </div>


       <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    
    <script>	
 
      function init_map() {
		var var_location = new google.maps.LatLng(41.14090,-73.26731);
 
        var var_mapoptions = {
          center: var_location,
          zoom: 14,
		      disableDefaultUI: true,
			     scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false
        };
 
		var var_marker = new google.maps.Marker({
			position: var_location,
			map: var_map,
			title:"VisionMix"});
 
        var var_map = new google.maps.Map(document.getElementById("map"),
            var_mapoptions);
 
		var_marker.setMap(var_map);	
 
      }
 
      google.maps.event.addDomListener(window, 'load', init_map);
 
    </script> 


<footer>
<?php include "seesomethingyoulike.php" ?> 
<?php include "footer.php" ?> 
  
      
 