<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">   
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="adp_ebook.php">   <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        
                      </a>
                      
                      <a class="next" href="irc.php">  <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Halle Berry Fragrances</h2>
				<span class="redline"></span>
				<h1>Picking up the ball and running with it – to everyone’s delight.</h1>
				
                
                <a class="btn btn-casestudy btn-reverse" href="http://www.halleberryfragrances.com/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                
			</div>
            
            
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat17_halle.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>One of the toughest challenges clients often face is changing agencies mid-stream on a site that’s already underway or in need of an update. Most developers cringe at the thought too. When Coty needed to find a new team to take over its Halle Berry Fragrances site – a complex Drupal site with language translations and multiple brand styles – team VMX got the call. We’re glad we did.</p>
				<p>As initially designed, the site reflected the look and feel of the three initial fragrances it was intended to showcase. Though aesthetically pleasing, the design presented difficulties as more fragrances were added to the brand’s expanding portfolio.</p>
				<p>The first task we sought to accomplish was transitioning the overall navigational framework to a more generic, “House of Halle” design approach such that it could accommodate each new fragrance. This updated framework was then applied across the site, integrating with existing design elements and providing a solid foundation for subsequent fragrance additions.</p>
				<p>Our second goal was to make the site easier to update. The lower menu element on the site’s homepage was developed using a complex Adobe Flash / jQuery hybrid solution. We transitioned this to a fully jQuery powered component that was infinitely more flexible.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
					</ol>

					<div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="./img/CS_csl_Halle1.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Halle2.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Halle3.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Halle4.png">
          </div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Took over the existing codebase to assume responsibility for ongoing site updates and maintenance tasks.</li>
    			<li>Modified the existing site homepage design and 3-product site framework to accommodate the integration of new products.</li>
    			<li>Designed and developed 2 all-new fragrance segments, each with their own unique look and feel.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Since taking over the site in 2010, VMX has overseen all site maintenance, various local market rollouts, and supported the launch of two additional Halle Berry fragrances – including the development of other campaign assets such as developing Facebook assets and email templates.</li>
    			<li>We are currently working with Coty to explore a full-site overhaul, which will likely entail migration to Coty’s new Drupal 7 platform and the integration of a responsive design navigational shell.</li>
    		</ul>
    	</div>
    </div>
    
        	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> 
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="adp_ebook.php">   <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="irc.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
   
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb09_beyonce.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./beyonce.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - Beyoncé Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
