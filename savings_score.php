<!DOCTYPE html>

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
		$page = 'page1';
		include "./header.php" 
	?>
    
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="itt_es.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="aetna-plan.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - My Savings Score / State of Savings</h2>
				<span class="redline"></span>
				<h1>Measuring America’s retirement readiness: Two online tools designed to help users track their progress towards a more secure retirement.</h1>
                
				  <a class="btn btn-casestudy btn-reverse" href="http://voyastateofsavings.voya.com/mysavingsscore/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                  
			</div>
            
          
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat06_voya-score.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>The Voya Retirement Research Institute collects knowledge, insights and information to help customers be retirement ready. In advance of National Savings Week, Voya and the RRI were looking for an engaging way to illustrate some key findings relative to how individual states matched up in terms of retirement readiness. This same data was also used to develop a simple metric &ndash; a “Savings Score” &ndash; by which users could quickly assess their own individual state of readiness.</p>
				<p>Team VMX worked with Voya to design and develop two companion online tools:</p>
				<ul>
					<li>The "State of Savings" interactive map, highlighting state-by-state statistics on retirement readiness</li>
					<li>The "My Savings Score" retirement calculator, allowing users to quickly measure their retirement readiness and then receive tips for improving their score</li>
				</ul>
				<p>Both tools were developed using HTML / jQuery and were based on the Drupal CMS platform in accordance with Voya web standards. The Savings Score tool was also developed to be responsive, allowing mobile users to use the calculator as well.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>
					<!-- Carousel indicators -->
<ol class="carousel-indicators">
        <li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
        <li data-target="#casestudycarousel" data-slide-to="1"></li>
        <li data-target="#casestudycarousel" data-slide-to="2"></li>
        <li data-target="#casestudycarousel" data-slide-to="3"></li>
        <li data-target="#casestudycarousel" data-slide-to="4"></li>
      </ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_SAVSCORE1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_SAVSCORE2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_SAVSCORE3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_SAVSCORE4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_SAVSCORE5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed and developed a reusable interface framework for the tools, ensuring continuity of user experience.</li>
    			<li>Developed an engaging, desktop optimized experience for the interactive map component using a combination of static HTML and jQuery.</li>
    			<li>Developed a Drupal based calculator that allowed users to determine their specific Savings Score based on data points and calculations that can be modified via an administrative interface.</li>
    			<li>Integrated an anonymous data-collection capability into the Savings Score tool that allows Voya to gather aggregate data for continued refinement of savings metrics.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Team VMX continues to work seamlessly with Voya brand and IT team members to maintain the online tools and their hosting environments.</li>
    		</ul>
    	</div>
    </div>
    
    
         	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="itt_es.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="aetna-plan.php">  <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
   
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb12_voya-myom.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./myorangemoney.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - MY ORANGE MONEY</h2>
					<span class="smallheaderdivider"></span>
					<p>Website launch promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">XYLEM WATERMARK</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
