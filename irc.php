<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left">
				<table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="halle_berry.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        <a class="next" href="voya-ppt.php">  <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                       </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>IRC History Presentation</h2>
				<span class="redline"></span>
				<h1>Helping a non-profit share their 75-year history of helping others.</h1>
				<a href="#" class="btn btn-casestudy btn-reverse" data-toggle="modal" data-target="#videomodal">View Video</a>
			</div>
                        
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat18_irc.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>In support of their 75th anniversary, the International Rescue Committee (IRC) was looking to develop a dynamic and engaging way of communicating the group’s storied history of bringing aid to those in need. Working in conjunction with a design team who was creating a companion print piece, VMX designed and developed an animated video-based presentation that told the tale … year-by-year and crisis-by-crisis.</p>
				<p>Using a graphic timeline as the central element, we broke the relevant information down by decade &ndash; from the 1920’s through to the 2000’s. We created an animated introductory segment for each decade followed by content blocks for each specific milestone. Using the static photography used in the print piece, we created animated sequences for each milestone by slowly tracking across multiple visuals, telling a unique story with each element.</p>
				<p>Graphic quote elements from committee leaders and famous historical figures were also added throughout the piece, providing additional historical context and visual interest.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_IRC1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_IRC2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_IRC3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_IRC4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_IRC5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed an animated timeline theme and visual setting that served as the primary backdrop for the full piece, conveying the key IRC milestones and as well as the historical context in which they took place.</li>
    			<li>Worked with a content team to isolate text and visual elements to support each of those milestones.</li>
    			<li>These elements were then combined to create a seamless, flowing animation loop that takes the viewer through each decade of the IRC’s history &ndash; from 1940 to 2012.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The completed video presentation &ndash; which runs on a continual 15-minute loop &ndash; was placed on a plasma screen in the lobby of the IRC’s New York headquarters.</li>
    		</ul>
    	</div>
    </div>
    
       	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left">
				<table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">  <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="halle_berry.php">  
                        <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
                        </a>
                        
                        <a class="next" href="voya-ppt.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb10_itt-fluid.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./itt_fluid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">ITT Fluid Technologies</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive capabilities presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb11_aetna-vid.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./aetna-video.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna Financial Wellness</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal employee benefits promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb15_voya-jane.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./itt_fluid.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya "Jane" Videos</h2>
					<span class="smallheaderdivider"></span>
					<p>National Sales meeting video opener</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
   


<!-- MODALS --> 

<!-- modal -->
<div id="videomodal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/closebutton-video.png"></button>
        <h4 class="modal-title">VisionMix</h4>
      </div>
      <div class="modal-body">
				<div id="video3">
					<div id="youtubevideo" class="video-responsive">
						<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/647U_4fN4lk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<script type="text/javascript">
$(function() {

  $("#videomodal").on('hidden.bs.modal', function (e) {

    // needed to find some way to remove video and then replace it because IE would close the div but continue to play sound


    $('iframe').hide();   // must hide the YouTube iframe first or closing it before playing will cause a black window in IE


    $('#video3').hide('fast');  // then hide the container div that has the video div that has the YouTube iframe in it

    var bringback = $("#youtubevideo").clone(true);   //clone the div that has the YouTube iframe and assign it to a variable

    $("#youtubevideo").remove(); // remove the div that has the YouTube iframe

    $("#video3").html(bringback);   // replace or recreate the div that has the YouTube iframe using the variable with cloned information
    });

    $("#videomodal").on('shown.bs.modal', function (e) {
        $('iframe').show();   // show the YouTube iframe                         
         $('#video3').show('fast');   // show the div that contains the YouTube video div
    });
}); 
</script>
 
