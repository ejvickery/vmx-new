<div class="container fadein">

	<div class="row text-center ">
		<h2 class="smallheader">Selected Clients</h2>
		<span class="smallheaderdivider"></span>
	</div>

	<div class="row clients">

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_coty">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_voya">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_chase">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_aetna">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_halle">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_mc">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_nautica">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_genre">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_xylem">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_balenc">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_prudent">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_exelis">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_guess">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_stetson">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_adp">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cl-box cl_odyssey">
			</div>
		</div>

	</div>

</div><!--container-->    