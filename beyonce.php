<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
   <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="glgf.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
                        <a class="next" href="itt_fluid.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Beyoncé Parfums</h2>
				<span class="redline"></span>
				<h1>Taking the reins: Team VMX takes over an ailing web property and gives it new life.</h1>
				 <a class="btn btn-casestudy btn-reverse" href="http://www.beyonceparfums.com/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                 
			</div>
            
           
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat09_beyonce.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>It’s a simple rule of business &ndash; clients change vendors, and for a wide variety of reasons. Taking over a project where a previous team left off is never easy, but addressing client needs is what we do.</p>
				<p>Coty’s existing Beyoncé Parfums website &ndash; a complex Drupal site with language translations and multiple brand styles &ndash; was experiencing technical and management issues that needed to be addressed. Team VMX was initially tasked with taking over site maintenance responsibilities in early 2014. Once the site was stabilized, the brand team asked us to explore ways in which the site could be modernized &ndash; both in terms of functionality and its overall look and feel. With budget always an issue, the challenge was to update the site without entirely rebuilding it.</p>
				<p>Our goal was to give the site a "facelift", keeping the existing site structure and user interface in place while updating its general appearance. We developed an entirely new visual design that was more in line with other Beyoncé web properties. Along the way, we added new fragrances to the lineup, retired some old ones, and developed a graphic email template to help drive traffic to the site.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
						<li data-target="#casestudycarousel" data-slide-to="6"></li>
					</ol>

        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="./img/CS_csl_Beyonce1.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce2.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce3.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce4.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce5.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce6.png">
          </div>
          <div class="item">
            <img class="img-responsive" src="./img/CS_csl_Beyonce7.png">
          </div>
        </div><!-- Carousel nav - quote marks -->
                          <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Took over the existing codebase to assume responsibility for ongoing site updates and maintenance tasks.</li>
    			<li>Modified the existing site homepage and internal page templates with updated visual design.</li>
    			<li>Deployed 3 new fragrances to the site complete with local market content and language translations.</li>
    			<li>Designed and developed graphic email template to be used for new product announcements.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>VMX happily continues to oversee all site maintenance and content administration across all local markets.</li>
    		</ul>
    	</div>
    </div>
    
    	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="glgf.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="itt_fluid.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

    
    </div><!--/container-->
    
    
   <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb08_glgf.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./glgf.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - Multibrand Site</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for holiday fragrance promotion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">COTY - Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
