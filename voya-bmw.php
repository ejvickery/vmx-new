<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev">
							<a class="previous" href="aetna-ae.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/><img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
		          <a class="next" href="voya-enroll-vid.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/><img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a>
		        </td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - Benchmark Wizard</h2>
				<span class="redline"></span>
				<h1>Making it easier for retirement plan administrators to see how they measure up to the competition</h1>
        <!--a class="btn btn-casestudy btn-reverse" href="http://visionmix.com/" target="_blank">Visit Site</a-->
			</div>
          
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat22_VoyaBMW.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>In 2005, Voya Financial (then ING) engaged team VMX to develop a tool to automate the process of developing retirement plan benchmark reports – essentially comparing a given plan to others within its business / industry sector based on various available data resources.</p>
				<p>The goal of the project was to allow ING plan administrators and TPAs to easily generate the report documents as part of their annual plan review process. These PDF formatted reports could also be used as a sales tool – highlighting plan deficiencies or inconsistencies for which ING could provide valuable solutions. The tool was a huge success, becoming an invaluable resource for the company.</p>
				<p>In 2015, Voya engaged VMX to help modernize and update the underlying technology that the tool was built on. We transitioned from the existing Adobe Flash and ASP.NET architecture to an open-source HTML and PHP solution, making the tool easier to update and migrate in-house should Voya want to do so. While mobile use was not the focus of the rebuild, a significant percentage of users were believed to be be accessing the tool via tablet, so basic responsive capabilities were a must.</p>
				<p>The result was an almost entirely new tool, ready for the next phase of its ongoing success.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_VoyaBMW_01.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaBMW_02.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaBMW_03.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaBMW_04.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Rebuilt the original application using a combination of HTML, CSS3 and jQuery for front-end development and PHP/MySQL for database communication.</li>
    			<li>Performed basic visual updates to conform to Voya’s new branding / visual design guidelines.</li>
    			<li>Implemented an easier / more efficient data update process based that leveraged the upadated technology approach.</li>
    			<li>Implemented analytics / metrics reporting capabilities so that Voya could track overall usage.</li>
    			<li>Added basic “luggage lock” security measure to restrict outside access to the tool.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The updated site has been even more successful than the previous iteration – with metrics and analytics showing steady increases in traffic since launch.</li>
    			<li>The move to an HTML / PHP architecture has reduced developed times for minor updates.</li>
    			<li>The new development approach has also made the tool more stable, virtually eliminating server errors what were becoming an increasing concern in the aging ASP.NET version.</li>
    		</ul>
    	</div>
    </div>
    
    
    <div class="CS-nav CS-nav-bottom">
			<div class="container" style="padding: 15px;">
				<div class="pull-left"><table>
						<tr>
							<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
						 <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
							<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
						</tr>
					</table>
				</div>
				<div class="pull-right"><table>
						<tr>
							<td class="CS-navtext browse">BROWSE</td>
							<td class="nextprev">
								<a class="previous" href="aetna-ae.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/><img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
					 			<a class="next" href="voya-enroll-vid.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/><img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a>
					 		</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
    
    
  </div><!--/container-->
    
    
 
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>
    
		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb04_mc-elearn.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./mc_elearning.php">
<table><tbody><tr><td>
					<h2 class="smallheader">MasterCard e-Learning</h2>
					<span class="smallheaderdivider"></span>
					<p>Distance learning module for LMS integration</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb06_voya-score.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./savings_score.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Voya My Savings Score / State of Savings</h2>
					<span class="smallheaderdivider"></span>
					<p>Online calculator and interactive map component</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
