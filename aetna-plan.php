<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
   <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="savings_score.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="glgf.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Aetna - Plan Highlights</h2>
				<span class="redline"></span>
				<h1>Coordinating traditional and interactive media to communicate employee benefits in a fresh and engaging fashion.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat07_aetna-plan.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>For its largest retirement plan sponsors, Voya provides custom communications geared towards helping the sponsor’s employees get the most out of their retirement benefits plan. As one would expect, finding an engaging and cost effective way to do this is a top priority.</p>
				<p>Throughout its 12-year working relationship with Voya (much of it done under the company’s previous ING moniker), VMX has been tasked with creating dynamic ways to improve both internal and participant facing communications. When the time came to update materials for their Aetna plan sponsor client, Voya wanted to create a unified communication toolkit, combining both print and interactive media elements.</p>
				<p>VMX started the process by developing a dynamic, interactive e-book component that allowed users to navigate through plan benefit and retirement savings educational materials. The final piece was housed within Aetna employee intranet and provided access to a wide variety of additional information and related resources.</p>
				<p>We then created a print companion piece that shared the overall look and feel of the interactive material, referencing much of the same key topics and resources. In this way, users could access the content in whichever way they felt most comfortable with &ndash; yet could easily move from format to format and find themselves in familiar territory.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan5.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Aetna-plan6.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Working with supplied brand guidelines, we designed and developed a rich and engaging visual framework for both print and interactive versions of the material.</li>
    			<li>We worked hand-in-hand with the Voya account team to map out and define appropriate content elements to be used within each format.</li>
    			<li>We developed a reusable, standardized navigational template for the e-book component &ndash; one that would facilitate development of e-books for subsequent client deployments.</li>
    			<li>We worked directly with Aetna’s IT team members to ensure a seamless integration with their intranet platform.</li>
    			<li>For the print component, we provided both print ready files and digital source materials &ndash; coordinating with Voya internal print resources to ensure a smooth print production process.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Following the successful deployment of the initial e-book and print companion, VMX worked with Voya to develop a secondary, audience-specific subset of the initial material.</li>
    			<li>Team VMX continues to maintain and update both versions of the material for Voya’s Aetna account team.</li>
    		</ul>
    	</div>
    </div>
        
        	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="savings_score.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="glgf.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->


    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb03_exelis.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./exelis.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Exelis - Print</h2>
					<span class="smallheaderdivider"></span>
					<p>Print and collateral material design collection</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb16_adp.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./adp_ebook.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - ADP E-book</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive retirement planning guide</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE Presentation</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
