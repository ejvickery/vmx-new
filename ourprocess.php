<div class="container">
	<div class="row text-center">
		<h2 class="smallheader">Our Process</h2>
		<span class="smallheaderdivider"></span>
	</div>

	<div class="row text-center">
		<div class="col-xs-12">
			<h1>WE <span class="red">LISTEN.</span> WE <span class="red">THINK.</span> WE <span class="red">SOLVE.</span></h1>
			<p>It sounds so simplistic, but our approach to developing meaningful client solutions is straight forward and pragmatic.</p>
		</div>
	</div>
	
	<div class="row">
		<div class="proc-lft col-sm-12 col-md-4">
			<h2 class="stepsheader"><span class="red">STEP 1:</span> DISCOVERY & DEFINITION</h2>
			<p>First, we talk about your needs. Preconceived ideas - ours or yours - about what will or will not work have the effect of shutting off dialog and limiting options that might actually be useful. We understand that every business has its own specific needs and a solution tailored to one situation may not apply to another, though they may appear similar on the surface.</p>
			<p>An initial discovery process establishes a dialogue so that we can learn about your unique needs and gather the necessary input that will allow us to move forward, together. Specifically, we’ll develop a comprehensive requirements document that clearly maps out what you need - and what we need - to get the job done.</p>
		</div>
		<div class="proc-cnt col-sm-12 col-md-4">
			<h2 class="stepsheader"><span class="red">STEP 2:</span> SOLUTION DESIGN</h2>
			<p>Once we have a handle on the task at hand, we’ll work with you to see how best to tackle the problem. Based upon our discovery session, team members contribute to analyzing your unique situation and developing potential solutions and multiple approaches to meeting your specific needs. It is never easy but we thrive on it.</p>
			<p>We’ll think through site architectures, user experience, layout options, front-end and back-end functionality requirements. We'll develop several directions and work up multiple design options that will let you help shape the end result. You tell us what you like, and what you don't and we walk away with a chosen direction to pursue.</p>
		</div>
		<div class="proc-rgt col-sm-12 col-md-4">
			<h2 class="stepsheader"><span class="red">STEP 3:</span> EXECUTION & VALIDATION</h2>
			<p>With a finalized plan of attack - from flow charts to wireframes to visual designs - we launch our development process. Initial mockups will be created and presented for your review. We will guide you through each step of the process, allowing you to track our progress and help make course corrections along the way.</p>
			<p>We will thoroughly test and measure our progress against the goals and success factors you set forth.</p>
			<p>Once the finish line is in sight, we’ll help finalize all the details and get your project ready to meet the world. Only when you are satisfied do we view the work as complete.</p>
		</div>
	</div>
<div class="clearfix" style="padding-bottom: 70px;"></div>
</div>
