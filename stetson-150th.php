<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-ebooks.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="aetna-ae.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Coty - Stetson 150th Tour</h2>
				<span class="redline"></span>
				<h1>Helping a longtime friend celebrate its 150th birthday in style &ndash; all across the USA</h1>
				
                  
            <a class="btn btn-casestudy btn-reverse" href="http://stetsoncologne.com/legend-lore/stetson_150th_tour/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                
			</div>
          
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat24_Stetson150.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Building off the success of the 2014 launch of its new site, Coty’s Stetson Cologne team was looking for a way to capitalize on the parent brand’s upcoming 150-year anniversary celebration. Given the rich American feel to the brand &ndash; and its strength in the heartland of the country &ndash; the decided to take Stetson Cologne on the road &hellip; literally.</p>
				<p>Working with a team of designers and vintage auto-restorers, they found a classic Ford F-150 and brought it back to its glory days &hellip; but with a decidedly Stetson twist. The Coty team then planned a multi-state tour, taking the newly restored Stetson truck to state fairs, rodeos and even a few Wal-Marts all across the Midwest. At each stop, the teams brand ambassadors invited visitors to check out the truck and sample Stetson Cologne.</p>
				<p>In support of the campaign, team VMX developed an interactive map that provided details for each stop along the tour. As each event took place, we added photos sent to us by the brand ambassadors &ndash; allowing visitors to log onto the site and see their photos. The resulting gallery consists of over 100 images and is still growing!</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Stetson150_01.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Stetson150_02.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Stetson150_03.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Stetson150_04.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Stetson150_05.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed and developed the interactive map &ndash; allowing users to preview each stop along the tour.</li>
    			<li>Developed a new homepage callout for the revolving hero banner and integrated new video campaign elements in order to generate buzz around the events and drive traffic to the map page.</li>
    			<li>Designed and developed a graphic email template to announce the campaign.</li>
    			<li>Worked with the brand team to upload new photos to the campaign image gallery after each stop along the tour.</li>
    			<li>In keeping with the rest of the site, we built the site using a responsive design HTML and jQuery development approach, providing a seamless user experience from desktop to tablet to mobile.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Working within tighter than usual timeframes &ndash; approximately 1 week from approval to launch! &ndash; we completed the map element, had the launch email deployed and first event photos posted.</li>
    			<li>The events drew huge crowds, and the media elements collected at each stop far exceeded what was anticipated. As a result, Team VMX had to rework the gallery component mid-campaign to accommodate the increased amount of assets.</li>
    			<li>Post-campaign, the map element is being redeployed to allow users to easily find their archived photos &hellip; still drawing traffic to the brand site content elements.</li>
    		</ul>
    	</div>
    </div>
    
    
           	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-ebooks.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="aetna-ae.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
    
 
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>
    
		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb13_balenciaga.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./balenciaga.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Balenciaga Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Luxury fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
