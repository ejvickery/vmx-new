<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">    
                      <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
           </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="aetna-video.php"> <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="balenciaga.php">  
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - My Orange Money</h2>
				<span class="redline"></span>
				<h1>Building the buzz: Introducing the benefits of Voya’s new web tool in a brief teaser video.</h1>
				<a href="#" class="btn btn-casestudy btn-reverse" data-toggle="modal" data-target="#videomodal">View Video</a>
			</div>
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat12_voya-myom.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>In 2014, Voya was finalizing the development of their new “My Orange Money” web tool, designed to shift the focus from thinking about the importance of savings today (a sometimes difficult concept to sell) to the more tangible concept of everyday income needs in retirement. The tool uses a dollar bill visual as it central interface component, representing the user’s retirement income needs, their income sources and any identified shortfall. Users are prompted to enter their personal information and then try various scenarios to “fill” their dollar.</p>
				<p>As Voya prepared for the release of this new tool, they wanted to generate excitement &ndash; both within their existing client base and across the wider industry. VMX was asked to develop a brief video teaser, highlighting the tool’s benefits and also showing features of the interface. The challenge to this was that much of the tool’s core functionality was still in the final stages of development.</p>
				<p>Working from static screenshots and mockups, we developed the video’s script and storyboard. Wherever possible, our plan was to let the tool itself be the hero. Our voiceover narration and screen visuals were designed to highlight the primary areas of the tool &ndash; simulating functionality in order to illustrate how it was intended to work. We combined these segments with stock video footage to create a fluid, cohesive narrative.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_Voya-MyOM1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-MyOM2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-MyOM3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-MyOM4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_Voya-MyOM5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with the Voya marketing team to develop the script and storyboard.</li>
    			<li>Developed an overall visual design to be used throughout the piece.</li>
    			<li>Used supplied screen static visuals to simulate the tool’s various features and functionality.</li>
    			<li>Recorded the video narration and sourced all stock video clips and music track to be used.</li>
    			<li>Composited all elements together in Adobe After Effects to create the final video.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The completed piece was used by Voya’s marketing team on the company’s website and YouTube channels.</li>
    			<li>We developed a second variation of the teaser, showcasing the slightly different user experience for a secondary audience.</li>
    			<li>Once completed, we provided the final source files to the Voya internal team, allowing them to perform ongoing updates on an as-needed basis.</li>
    		</ul>
    	</div>
    </div>
    
        	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> 
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="aetna-video.php">                        <img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/>
</a>

<a class="next" href="balenciaga.php">   
                       <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/>
                        </a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
   
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb11_aetna-vid.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./aetna-video.php">
<table><tbody><tr><td>
					<h2 class="smallheader">AETNA - WELLNESS VIDEO</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal employee benefits promotional video</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a> 
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb18_irc.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./irc.php">
<table><tbody><tr><td>
					<h2 class="smallheader">IRC HISTORY PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Internal company history video presentation</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
      </div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb19_voya-ppt.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./voya-ppt.php">
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - SEE PRESENTATION</h2>
					<span class="smallheaderdivider"></span>
					<p>Global rebrand of seminar PowerPoint presentations</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  

<!-- MODALS --> 

<!-- modal -->
<div id="videomodal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/closebutton-video.png"></button>
        <h4 class="modal-title">VisionMix</h4>
      </div>
      <div class="modal-body">
				<div id="video3">
					<div id="youtubevideo" class="video-responsive">
						<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/gZVOnucRxyM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<script type="text/javascript">
$(function() {

  $("#videomodal").on('hidden.bs.modal', function (e) {

    // needed to find some way to remove video and then replace it because IE would close the div but continue to play sound


    $('iframe').hide();   // must hide the YouTube iframe first or closing it before playing will cause a black window in IE


    $('#video3').hide('fast');  // then hide the container div that has the video div that has the YouTube iframe in it

    var bringback = $("#youtubevideo").clone(true);   //clone the div that has the YouTube iframe and assign it to a variable

    $("#youtubevideo").remove(); // remove the div that has the YouTube iframe

    $("#video3").html(bringback);   // replace or recreate the div that has the YouTube iframe using the variable with cloned information
    });

    $("#videomodal").on('shown.bs.modal', function (e) {
        $('iframe').show();   // show the YouTube iframe                         
         $('#video3').show('fast');   // show the div that contains the YouTube video div
    });
}); 
</script>


  
