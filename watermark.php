<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>

  
       	<div class="CS-nav ">
		<div class="container" style="padding: 15px;">
			<div class="pull-left">
				<table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study">
                        
                        <!--<img alt="" src="/img/return.png" class=" img-responsive">-->
                        
                       <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="browse CS-navtext">BROWSE</td>
						<td class="nextprev">
							<a class="previous" href="exelis.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           		<img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           		<a class="next" href="stetson.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           		<img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a>
           	</td>
					</tr>
				</table>
			</div>
		</div>
	</div>



	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Xylem - Watermark</h2>
				<span class="redline"></span>
				<h1>Helping a corporate philanthropy organization spread their message online &ndash; both inside and out</h1>
				
                 <a class="btn btn-casestudy btn-reverse" href="http://www.xylemwatermark.com/" target="_blank">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
                
			</div>
            
           
            
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img alt="" class="img-responsive centerup" src="./img/CS_feat02_watermark.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Xylem Watermark is a corporate philanthropy initiative dedicated to bringing impoverished nations and people in need the most basic human need &ndash; clean, potable water. Xylem (formerly ITT Fluid Technologies) is uniquely suited to the task, leveraging their equipment solutions and industry leading expertise to address the growing global water crisis.</p>
				<p>Working in tandem with the organization’s PR firm in late 2010, team VMX designed and developed a comprehensive web experience targeted at both external and internal audiences, spreading the message and seeking help. In 2014, VMX worked with Xylem to update this existing site, implementing a new custom theme utilizing responsive design techniques.</p>
				<p>Developed on the WordPress CMS platform, the completed site provides detailed information on key water issues, Xylem’s commitment, and details on their partner programs. It also contains a blog forum &ndash; dubbed “running water” &ndash; where Xylem executives and employees discuss relevant issues and provide details on their fieldwork. There is also an exhaustive repository of communications materials that can help Xylem employees generate awareness and plan local Watermark-oriented events.</p>
				<p>Beyond the basic site components, VMX has also developed a comprehensive employee volunteer trip application and provided a mechanism to donate to water-related charities.</p>
			</div>
		</div>
        
       
       
       
       
        
        
        

    <div class="row">
    
  
    
    
    
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer">
				<div id="casestudycarousel" class="carousel slide" >
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img alt="" class="img-responsive" src="./img/CS_csl_Watermark1.png"/>
						</div>
						<div class="item">
							<img alt="" class="img-responsive" src="./img/CS_csl_Watermark2.png"/>
						</div>
						<div class="item">
							<img alt="" class="img-responsive" src="./img/CS_csl_Watermark3.png"/>
						</div>
						<div class="item">
							<img alt="" class="img-responsive" src="./img/CS_csl_Watermark4.png"/>
						</div>
						<div class="item">
							<img alt="" class="img-responsive" src="./img/CS_csl_Watermark5.png"/>
						</div>
                        </div>
                     
                 <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img alt="" src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img alt="" src="/img/next.png" class="img-responsive"/>
  </a>
                        
            
                        
					</div>
                    
  </div></div>           



    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Designed and developed the WordPress site &ndash; allowing Xylem and their partners to manage their own content.</li>
    			<li>We built the site using a responsive design HTML and jQuery development approach, providing a seamless user experience from desktop to tablet to mobile.</li>
    			<li>Developed an interactive “Impact Map” that allows users to explore the specific crisis areas where Xylem is active and how they are making a difference.</li>
    			<li>Developed a multi-session employee trip application process supported by a custom WordPress plugin that allows the Watermark team to manage and review applications.</li>
    			<li>Worked with Watermark’s giving partner Benevity to provide a donation mechanism that supports multiple currencies and records transactional data.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Working under a tight timeframe, we launched the new site theme in conjunction with World Water Day 2014 &ndash; completing a full site re-skin in just over 4 weeks.</li>
    			<li>Since the launch of the re-skin, we have worked with Xylem and Cone to introduce numerous updates to the site’s hero area and content in alignment with scheduled events and topical news stories.</li>
    			<li>We play an active role in shaping the site’s continued development, working with Xylem and their partners to plan and strategize ongoing site edits and enhanced functionality components.</li>
    		</ul>
    	</div>
        
    </div>
    
    
       
       	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="browse CS-navtext">BROWSE</td>
						<td class="nextprev"><a class="previous" href="exelis.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           
           <a class="next" href="stetson.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    
    </div><!--/container-->
    
 
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb01_stetson.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./stetson.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Stetson Caliber</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for new brand launch</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb13_balenciaga.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./balenciaga.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Balenciaga Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Luxury fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
 
<footer>
  <?php include "seesomethingyoulike.php" ?>
  <?php include "footer.php" ?>
  
