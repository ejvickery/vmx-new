<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title><?php
    $page = 'page1';
    include "../header.php" ?>
</head>

<body>
  <div class="container-fluid navwrap">
    <div class="navbar-header" style="width:100%;">
      <a class="return pull-left" href="%3C?php$url%20=%20'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];%20?%3E/work.php"><img class="img-responsive" src="%3C?php$url%20=%20'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];%20?%3E/img/return.png"> RETURN</a> <a class="return pull-right" href="#"><img class="img-responsive" src="%3C?php$url%20=%20'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];%20?%3E/img/next.png"></a> <a class="return pull-right" href="#">BROWSE <img class="img-responsive" src="%3C?php$url%20=%20'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];%20?%3E/img/previous.png"></a>
    </div>
  </div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - ADP eBook</h2>
				<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
				<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--></a>
			</div>
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="../img/CS_feat16_adp.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<!-- Carousel indicators -->

					<ol class="carousel-indicators"></ol><!-- Carousel items -->

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="../img/CS_csl_ADP1.png">
						</div>

						<div class="item">
							<img class="img-responsive" src="../img/CS_csl_PLACEHOLDER.png">
						</div>

						<div class="item">
							<img class="img-responsive" src="../img/CS_csl_PLACEHOLDER.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					<a class="carousel-control left"></a> <a class="carousel-control right"></a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    			<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
    		</ul>
    	</div>
    </div>

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
    </div>

		<div class="row relatedprojects">

			<a href="./stetson.php"><div class="col-xs-12 col-sm-4" style="background: url('../img/CS_thumb01_stetson.jpg') no-repeat center; background-size: 100%"></div></a>

			<a href="./balenciaga.php"><div class="col-xs-12 col-sm-4" style="background: url('../img/CS_thumb13_balenciaga.jpg') no-repeat center; background-size: 100%"></div></a>

			<a href="./halle_berry.php"><div class="col-xs-12 col-sm-4" style="background: url('../img/CS_thumb17_halle.jpg') no-repeat center; background-size: 100%"></div></a>

		</div>

	</div>

  <?php include "../seesomethingyoulike.php" ?>
  <?php include "../footer.php" ?>
  
</body>
</html>
