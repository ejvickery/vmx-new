<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
  <?php include "overlayinclude.php" ?>


	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/>
                        </a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right">
				<table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev">
							<a class="previous" href="voya-bmw.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/><img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
							<a class="next" href="exelis.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/><img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Voya - Online Enrollment Promo</h2>
				<span class="redline"></span>
				<h1>Continuing the buzz: Adding another promotional video to our expanding Voya library</h1>
				<a href="#" class="btn btn-casestudy btn-reverse" data-toggle="modal" data-target="#videomodal">View Video</a>
			</div>
          
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat23_VoyaENROLLVID.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>Following our 2014 collaboration with Voya to help launch their revolutionary MyOrangeMoney retirement planning tool, team VMX has been asked to develop a wide range of similar promotional videos and website demonstrations.</p>
				<p>Our most recent addition to the library is this Online Enrollment Experience promotional piece, developed to coordinate with a press release and marketing campaign. The overall look and feel leverages the original MyOrangeMoney promotional video we created, integrating seamlessly with the other pieces in the library. Working from static screenshots and mockups, we built out the screen animations to support the script elements. As Voya’s toolsets increasingly embrace responsive design techniques, the screen simulations highlight the Online Enrollment Experience’s mobile and tablet interfaces – reinforcing the ease and availability of the tool itself.</p>
				<p>As always, our approach was to let the tool itself be the hero – referencing screens and animations as much as possible through out the piece. Voiceover narrations and screen visuals were designed to showcase key areas and functions of the tool. Where possible, we simulate screen functionality to further illustrate how specific tool areas work. Stock video footage, as well as animated logo bookends, was then added to create a fluid, cohesive narrative.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
						<li data-target="#casestudycarousel" data-slide-to="5"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_01.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_02.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_03.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_04.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_05.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_VoyaENROLLVID_06.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Worked with the Voya marketing team to develop the script and storyboard.</li>
    			<li>Developed an overall visual design to be used throughout the piece.</li>
    			<li>Used supplied screen static visuals to simulate the tool’s various features and functionality.</li>
    			<li>Recorded the video narration and sourced all stock video clips and music track to be used.</li>
    			<li>Composited all elements together in Adobe After Effects to create the final video.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>The completed piece was used by Voya’s marketing team on the company’s website and YouTube channels.</li>
    			<li>Once completed, we provided the final source files to the Voya internal team, allowing them to perform ongoing updates on an as-needed basis.</li>
    		</ul>
    	</div>
    </div>
    
    
           	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"> <img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev">
							<a class="previous" href="voya-bmw.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/><img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
							<a class="next" href="exelis.php"> <img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/><img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    
    </div><!--/container-->
    
    
 
    
    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>
    
		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: 100%">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb13_balenciaga.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./balenciaga.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Balenciaga Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Luxury fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb17_halle.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./halle_berry.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Halle Berry Fragrances</h2>
					<span class="smallheaderdivider"></span>
					<p>Celebrity fragrance brand website design / development</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    

<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
   


<!-- MODALS --> 

<!-- modal -->
<div id="videomodal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/closebutton-video.png"></button>
        <h4 class="modal-title">VisionMix</h4>
      </div>
      <div class="modal-body">
				<div id="video3">
					<div id="youtubevideo" class="video-responsive">
						<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/YnGzm6l8JeA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<script type="text/javascript">
$(function() {

  $("#videomodal").on('hidden.bs.modal', function (e) {

    // needed to find some way to remove video and then replace it because IE would close the div but continue to play sound


    $('iframe').hide();   // must hide the YouTube iframe first or closing it before playing will cause a black window in IE


    $('#video3').hide('fast');  // then hide the container div that has the video div that has the YouTube iframe in it

    var bringback = $("#youtubevideo").clone(true);   //clone the div that has the YouTube iframe and assign it to a variable

    $("#youtubevideo").remove(); // remove the div that has the YouTube iframe

    $("#video3").html(bringback);   // replace or recreate the div that has the YouTube iframe using the variable with cloned information
    });

    $("#videomodal").on('shown.bs.modal', function (e) {
        $('iframe').show();   // show the YouTube iframe                         
         $('#video3').show('fast');   // show the div that contains the YouTube video div
    });
}); 
</script>
 
