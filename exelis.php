<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>VisionMix | Case Studies</title>
  
  <?php
    $page = 'page1';
    include "./header.php" 
  ?>
   <?php include "overlayinclude.php" ?>
  

	<div class="CS-nav">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-enroll-vid.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="watermark.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row CS-headerBG">
			<div class="col-xs-12 col-sm-6 CS-leftpanel">
				<h2>Exelis - Print</h2>
				<span class="redline"></span>
				<h1>Working within &ndash; and pushing &ndash; the limits of corporate design to help a long-time client transition their branding with a new company name.</h1>
				
			</div>
            
            <!--<a class="btn btn-casestudy btn-reverse" href="#">Visit Site<!--<i class="fa fa-chevron-right"></i>--</a>-->
            
			<div class="col-xs-12 col-sm-6 CS-img">
				<img class="img-responsive centerup" src="./img/CS_feat03_exelis.jpg" />
			</div>
		</div>

		<div class="row CS-summarypanel">
			<div class="col-xs-12">
				<h2>PROJECT SUMMARY</h2>
				<p>As most designers with corporate clients will tell you, working within established brand guidelines is often a very restrictive and frustrating process. More often than not, it's just as challenging for your internal client, who is likely looking for something new and different.</p>
				<p>In each case, our goal is to work within the corporate guidelines &ndash; pushing the limits where possible &ndash; to create a piece that fits within the larger corporate family, yet stands out as a unique communication element.</p>
				<p>Over the last decade, team VMX has worked on numerous design projects for ITT Industries various divisions. From brochures and info-graphics, to tradeshow visuals and large-format poster graphics, we’ve done it all. In 2012, ITT broke its subdivisions into various smaller companies &ndash; each with their own unique name and brand standards.</p>
				<p>Working in conjunction with ITT’s longtime content specialist &ndash; Lazar Creative Services &ndash; team VMX is helping these divisions recast their existing brand communication materials. The example here reflects a typical brochure layout and related PowerPoint template completed for ITT Defense Electronics & Services that needed to be updated to fit within the new ITT Exelis brand standards.</p>
			</div>
		</div>

    <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center carouselcontainer" id="">
				<div id="casestudycarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
					<span class="CS-smallheader">Sample Images</span><br>

					<!-- Carousel indicators -->
					<ol class="carousel-indicators">
						<li data-target="#casestudycarousel" data-slide-to="0" class="active"></li>
						<li data-target="#casestudycarousel" data-slide-to="1"></li>
						<li data-target="#casestudycarousel" data-slide-to="2"></li>
						<li data-target="#casestudycarousel" data-slide-to="3"></li>
						<li data-target="#casestudycarousel" data-slide-to="4"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item active">
							<img class="img-responsive" src="./img/CS_csl_exelis1.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_exelis2.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_exelis3.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_exelis4.png">
						</div>
						<div class="item">
							<img class="img-responsive" src="./img/CS_csl_exelis5.png">
						</div>
					</div><!-- Carousel nav - quote marks -->
					                   <!-- Controls -->
  <a class="left carousel-control" href="#casestudycarousel" role="button" data-slide="prev">
    <img src="/img/previous.png" class="img-responsive"/>
  </a>
  <a class="right carousel-control" href="#casestudycarousel" role="button" data-slide="next">
    <img src="/img/next.png" class="img-responsive"/>
  </a>
				</div>
			</div><!--/col-->
    </div>
    
    <div class="row CS-summarypanel">
    	<div class="col-sm-12 col-md-6 CS-lowerleft">
    		<h2>WHAT WE DID</h2>
    		<ul>
    			<li>Developed a unique visual design approach that fits within the new company’s updated visual guidelines and standards.</li>
    			<li>We provided various initial approaches to content layout and revised to client direction through an iterative and collaborative design process.</li>
    			<li>We worked with in-house and external print production team members to ensure maximum quality of final product.</li>
    		</ul>
    	</div>
    	<div class="col-sm-12 col-md-6 CS-lowerright">
    		<h2>RESULTS</h2>
    		<ul>
    			<li>Once established, we worked with client stakeholders to translate the new visual design approach to meet various communication requirements &ndash; from print to PowerPoint.</li>
    			<li>We continue to design and develop print components for ITT’s offspring organizations.</li>
    		</ul>
    	</div>
    </div>
    
        	<div class="CS-nav CS-nav-bottom">
		<div class="container" style="padding: 15px;">
			<div class="pull-left"><table>
					<tr>
						<td class="hoverimage"><a class="back" href="./work.php#case-study"><img class="bottom" src="./img/BACK_rest.png" alt=""/> 
           <img class="top" src="./img/BACK_hot.png" alt=""/></a></td>
						<td class="CS-navtext"><a href="./work.php#case-study">return</a></td>
					</tr>
				</table>
			</div>
			<div class="pull-right"><table>
					<tr>
						<td class="CS-navtext browse">BROWSE</td>
						<td class="nextprev"><a class="previous" href="voya-enroll-vid.php"><img class="bottom" src="./img/ARW_PREV_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_PREV_hot.png" alt=""/></a>
           <a class="next" href="watermark.php"><img class="bottom" src="./img/ARW_NEXT_rest.png" alt=""/> 
           <img class="top" src="./img/ARW_NEXT_hot.png" alt=""/></a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    
    </div><!--/container-->
    
  


    <div class="container">

    <div class="row text-center">
      <h2 class="smallheader">Related Projects</h2>
			<span class="smallheaderdivider"></span>
    </div>

		<div class="row relatedprojects">

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb02_watermark.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./watermark.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Xylem Watermark</h2>
					<span class="smallheaderdivider"></span>
					<p>Website design / development for corporate non-profit</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb07_aetna-plan.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./aetna-plan.php">
<table><tbody><tr><td>
					<h2 class="smallheader">Aetna - Plan Highlights</h2>
					<span class="smallheaderdivider"></span>
					<p>Employee benefits guide print piece and electronic companion</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

			<div class="relatedblock col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: url('./img/CS_thumb16_adp.jpg') no-repeat center; background-size: cover">
				<a class="overlay" href="./adp_ebook.php"> 
<table><tbody><tr><td>
					<h2 class="smallheader">VOYA - ADP Ebook</h2>
					<span class="smallheaderdivider"></span>
					<p>Interactive retirement planning guide</p>
					<img class="mag-glass" alt="Magnifying Glass" src="./img/mag_glas_icon.png"/>
</td></tr></tbody></table>
				</a>
			</div>

		</div>

	</div>
    
<footer>
  <?php include "./seesomethingyoulike.php" ?>
  <?php include "./footer.php" ?>
  
