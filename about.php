<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>VisionMix</title>

<?php
$page = 'page2';
include "header.php" ?>
    

<div class="row colorblockheader redbg equalHeight">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ipadbg left">
		<table>
			<tbody>
				<tr>
					<td><h1><span class="red">We’re VisionMix. </span><span class="white">Digital craftsmen. Think Stradivarius, but with pixels.</span></h1></td>
				</tr>
			</tbody>
		</table>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
  	<table>
  		<tbody>
  			<tr>
  				<td><p class="italic">As a provider of fully integrated corporate communications solutions we are expert in handling assignments ranging from purely creative to those requiring technical specialization. Just think of us as a part of your team that is solely responsible for optimizing user experiences across your digital properties.</p></td>
  			</tr>
  		</tbody>
  	</table>
	</div>
</div>
      

<div class="container fadein">
<div class="row text-center ourapproach">

<h2 class="smallheader">Our Approach</h2>
<span class="smallheaderdivider"></span>

<h1>Thought, emotion, beauty ... <span class="red">it should work well too</span></h1>

<p class="text-left">VMX provides web design and digital media services to companies that need B2B, B2C or internal communications support. We pride ourselves on our ability to provide compelling creative and technical solutions that drive meaningful results and service beyond measure. We support clients ranging from traditional advertising and marketing agencies to direct client relationships with high profile companies as well as smaller companies and venture backed start-ups.
</p>

</div>

 <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12 text-center" style="margin-bottom:5%;">
      
      <a href="./whatwedo.php" class="btn btn-default">More on What We Do <!--<i class="fa fa-chevron-right"></i>--></a>
      
      </div><!--/col-->


</div>

<script>
$(document).ready(
    function(){

$('.mikebg').click(function( event ) {
	  $('.sambg').children().fadeIn('fast');
   $('.samarrow').hide();
   $('#sambio').slideUp('fast');
	
   $('.mikebg').children().fadeToggle();
   $('.mikearrow').show();
   $('#mikebio').slideToggle();
   
});

$('.sambg').click(function( event ) {
	
	  $('.mikebg').children().fadeIn('fast');
   $('.mikearrow').hide();
   $('#mikebio').slideUp('fast');
   
	
   $('.sambg').children().fadeToggle();
   $('.samarrow').show();
   $('#sambio').slideToggle();
});

$('#mikeclose').click(function( event ) {
	  $('.sambg').children().fadeIn('fast');
   $('.samarrow').hide();
   $('#sambio').slideUp('fast');
	
   $('.mikebg').children().fadeIn('fast');
   $('.mikearrow').hide();
   $('#mikebio').slideUp('fast');
   
});

$('#samclose').click(function( event ) {
	
	  $('.mikebg').children().fadeIn('fast');
   $('.mikearrow').hide();
   $('#mikebio').slideUp('fast');
   
	
   $('.sambg').children().fadeIn('fast');
   $('.samarrow').hide();
   $('#sambio').slideUp('fast');
});



 
 });
 
 
</script>

<div class="container" style="padding-bottom:5%;">
<div class="row text-center redbg" style="padding:0;">

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-left">
      <h1 class="misfits"><span class="white">A small, dedicated band of misfits. Scalable to infinity.</span></h1>
      </div>
      
<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 mikebg">
<h1>Michael Hilleary <span class="subhead">Principal / Creative Director</span></h1>
<img class="mikearrow bioarrow" src="./img/bioarrow.png"/>
<img class="plusbutton" src="./img/plusbutton.png"/>
</div>

<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 sambg">
<h1>Samantha Nagy <span class="subhead">Principal / Account Director</span></h1>
<img class="samarrow bioarrow" src="./img/bioarrow.png"/>
<img class="plusbutton" src="./img/plusbutton.png"/>
</div>
     

</div><!--row-->

<div class="row" id="mikebio">
<img id="mikeclose" class="closebutton" src="./img/closebutton.png"/>
<h1>Michael Hilleary <span class="subhead">Principal / Creative Director</span></h1>

<p>One of the trademarks of our company is our clean, effective, eye-catching design, and that’s all Michael. He is the single point of consistent beauty in everything VisionMix designs. Nothing leaves our company without Michael’s design eye and approval. His hands-on expertise comes from his 20+ years of experience with all facets of interactive design and development —  from initial concept and storyboard creation through to final production and product launch. He possesses a keen understanding of today’s media technologies and is fluent in all tools of the trade.</p>

<p>Throughout Michael’s career, he has successfully designed, art directed, and produced numerous interactive multimedia and web based applications for Fortune 500 clients including: ING/Voya Financial, Coty Cosmetics, ITT Fluid Technologies, Gen Re, Chase Paymentech, Pfizer, Cheesbrough Ponds, Aetna Financial Services and Unilever.</p>
</div>

<div class="row" id="sambio">
<img id="samclose" class="closebutton" src="./img/closebutton.png"/>
<h1>Samantha Nagy <span class="subhead">Principal / Account Director</span></h1>

<p>
Samantha is a highly motivated, creative individual who can’t be stopped. As with the entire VisionMix team, we work hard, play hard, and sleep well at night knowing we produce an outstanding product, and our clients are happy. Samantha’s job is to make sure that happens.</p>

<p>She has been involved in all aspects of the field, from the most basic web development all the way up to the top. We believe that experience makes a good manager, someone who understands the day-to-day life of the people they manage.</p>

<p>In the course of her career, she has worked on the creation of hundreds of multimedia projects for many companies large and small, such as Pfizer, Bayer, ING, Mass Mutual, Johnson & Johnson and IBM.
</p>


</div>

<div class="row equalHeight text-center" style="padding:0;">

<div class="teambg col-xs-12 col-sm-12 col-md-6 col-lg-6 text-left">
      
      </div>
      
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 greybg" style="padding-top:4%;">
<h2 class="smallheader white">Our Team</h2>
<span class="smallheaderdivider white"></span>

<p class="smallitalic text-left">
The VisionMix team is a group of exceptionally talented, bright people from a broad spectrum of creative and strategic disciplines. Each one of us is focused on providing you with the highest level of marketing and communications support. Beyond this core team, we maintain relationships with creative, marketing, and technology professionals - allowing us to meet the needs of any client engagement.
</p>

</div>
     

</div><!--row-->

</div><!--container-->


      
        
<footer>
<?php include "letsworktogether.php" ?> 
<?php include "footer.php" ?> 
  
      