<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40095098-2', 'auto');
  ga('send', 'pageview');

</script>

<script>
$('a.scroll').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 900);
    return false;
});
</script>

<script>
$( document ).ready(function() {

(function( $ ){

	var defaults = {
		leftOffset : 15,
		live : true
	};
	var data_attr = 'magicline-data';

	var methods = {

		init : function(options) {
  
			return this.each(function(){
				var $this = $(this),
					data = $this.data(data_attr);

	        	data = $.extend({}, defaults, data, options);

		        data.$mainNav = $this;
		        data.leftPos = data.newWidth = 0;
		      
		    	data.$mainNav.css('position', 'relative');
		    	data.$line = $('<li class="magic-line"></li>');
		    	data.$mainNav.append(data.$line);
		    	data.$current = data.$mainNav.find(".active a");
		      
				if (data.$current.length){
					data.newWidth = data.$current.width();
					data.leftPos = data.$current.parent().position().left + data.leftOffset;
				}

		      	data.$line
		      		.css('position', 'absolute')
					.width(data.newWidth)
					.css('left', data.leftPos)
					.data('origLeft', data.leftPos)
					.data('origWidth', data.newWidth);

				if (data.live){
					data.$mainNav.find('a').hover(function() {
						var $el = $(this);
						data.$line.stop().animate({
							left: $el.parent().position().left + data.leftOffset,
							width: $el.width()
						});
					}, function() {
						data.$line.stop().animate({
							left: data.$line.data('origLeft'),
							width: data.$line.data('origWidth')
						});
					});
				}

	        	$this.data(data_attr, data);
	     	});
		}
	};

	$.fn.magicLine = function( method ) {
	    // Method calling logic
	    if (!method) method = 'init';

	    if ( methods[method] ) {
	    	return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
	    } else if ( typeof method === 'object' || ! method ) {
	    	return methods.init.apply( this, arguments );
	    } else {
	    	$.error( 'Method ' +  method + ' does not exist on jQuery.magicLine' );
	    }

	};
})( jQuery );

// Initialization
$('#main-nav').magicLine();
});
</script>



<script>

$(window).scroll(function() {
  if ($(document).scrollTop() > 5) {
    $('.navbar-fixed-top').addClass('shadownav');
  } else {
    $('.navbar-fixed-top').removeClass('shadownav');
  }
});
</script>

<script>
$(document).ready(function() {
    // run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);
});

//Function to the css rule
function checkSize(){
    if ($(".screensizer").css("float") == "none" ){
       
	   $('.equalHeight').each(function() {
  var eHeight = $(this).innerHeight();

  $(this).find('div').outerHeight(eHeight);
});

	   
    }
}
</script>

<style>
.screensizer {float:left;}
@media only screen and (min-width: 990px){
	.screensizer {float:none;}
}
</style>


<script>
/**
* @author Glen Cheney
*/
/*
* jQuery throttle / debounce - v1.1 - 3/7/2010
* http://benalman.com/projects/jquery-throttle-debounce-plugin/
*
* Copyright (c) 2010 "Cowboy" Ben Alman
* Dual licensed under the MIT and GPL licenses.
* http://benalman.com/about/license/
*/
(function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);

// Shuffle Initialize:

/* $(function() {
$('#grid').shuffle({
     'itemSelector': '[class*="col-"]'
 });
});*/


var shuffleme = (function( $ ) {
  'use strict';

  var $grid = $('#grid'),
      $filterOptions = $('.filter-options'),
      $sizer = $grid.find('.shuffle_sizer'),

  init = function() {

    // None of these need to be executed synchronously
    setTimeout(function() {
      listen();
      setupFilters();
    }, 100);

    // instantiate the plugin
    $grid.shuffle({
      itemSelector: '[class*="col-"]',
      sizer: $sizer    
    });
  },

  // Set up button clicks
  setupFilters = function() {
    var $btns = $filterOptions.children();
    $btns.on('click', function() {
      var $this = $(this),
          isActive = $this.hasClass( 'active' ),
          group = isActive ? 'all' : $this.data('group');

      // Hide current label, show current label in title
      if ( !isActive ) {
        $('.filter-options .active').removeClass('active');
      }

      $this.toggleClass('active');

      // Filter elements
      $grid.shuffle( 'shuffle', group );
    });

    $btns = null;
  },
      
  // Re layout shuffle when images load. This is only needed
  // below 768 pixels because the .picture-item height is auto and therefore
  // the height of the picture-item is dependent on the image
  // I recommend using imagesloaded to determine when an image is loaded
  // but that doesn't support IE7
  listen = function() {
    var debouncedLayout = $.throttle( 300, function() {
      $grid.shuffle('update');
    });

    // Get all images inside shuffle
    $grid.find('img').each(function() {
      var proxyImage;

      // Image already loaded
      if ( this.complete && this.naturalWidth !== undefined ) {
        return;
      }

      // If none of the checks above matched, simulate loading on detached element.
      proxyImage = new Image();
      $( proxyImage ).on('load', function() {
        $(this).off('load');
        debouncedLayout();
      });

      proxyImage.src = this.src;
    });

    // Because this method doesn't seem to be perfect.
    setTimeout(function() {
      debouncedLayout();
    }, 500);
  };      
  
  return {
    init: init
  };
}( jQuery ));



$(document).ready(function() {
  shuffleme.init();
});






</script>

  
    
    <script src="<?php
$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>/js/bootstrap.min.js"></script>
    <script src="<?php
$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>/js/parallax.js"></script>

    
    

    
 
    
    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    
    
