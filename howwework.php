<div id="howwework" class="dotcheckbg">
	<div class="container">
      
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
				<h2 class="smallheader">How We Work With You</h2>
				<span class="smallheaderdivider"></span> 
			</div>

			<div class="col-xs-12 text-center">
				<h1><span class="white">Think of us as an</span> <span class="red">extension of your team.</span></h1>
			</div>
			
			<div class="col-xs-12 text-center">
				<p><span class="white">We transform your insights and well thought out strategic thinking into creative and compelling internal and external digital communications that deliver results that exceed your expectations.<br><br> Our only goal is to make your job easier. We can do that in whichever way best suits your needs - supporting internal resources, providing design services, serving as a development partner. We can work on a small component of a project as part of a larger team, or serve as your turnkey solution provider. We have experience supporting a wide variety of communications needs across multiple audiences and media channels.</span></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<img alt="" src="img/howwework_mntg_horiz.png" class="hidden-xs hidden-sm img-responsive centerup">
				<img alt="" src="img/howwework_mntg_vert.png" class="hidden-md hidden-lg img-responsive centerup">
			</div>
		</div>

  </div><!--/container-->
</div><!--/dotcheckbg-->