<?php

if(empty($_POST['name'])   ||    
   empty($_POST['email'])  ||
   empty($_POST['message'])||
   empty($_POST['organization'])||   
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))    
{     
     echo "No arguments Provided!";   return false; 
} 

	
$name = $_POST['name'];
$email_address = $_POST['email'];
$organization = $_POST['organization']; 
$message = $_POST['message'];
	
// create email body and send it	
$to = 'info@visionmix.com'; // put your email
$email_subject = "Form Submission:  $name";
$email_body = "New Contact Request from $name! \n\n".
				  "Email: $email_address\nOrganization: $organization\nMessage:\n$message";
$headers = "From: info@visionmix.com\n";
$headers .= "Reply-To: $email_address";	
mail($to,$email_subject,$email_body,$headers);
return true;			
?>